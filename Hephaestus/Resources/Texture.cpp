#include "Texture.h"
#include "../Gfx/Device.h"
#include "../Gfx/Buffer.h"
#include <stdexcept>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

std::shared_ptr<Texture> Texture::_empty;
bool Texture::_hasEmpty = false;

Texture::Texture(const char* filename) {
	create(filename);
}

Texture::~Texture() {
	cleanup();
}

void Texture::create(const char* filename) {
	stbi_set_flip_vertically_on_load(true);

	int width, height, channels;
	auto pixels = stbi_load(filename, &width, &height, &channels, STBI_rgb_alpha);

	VkDeviceSize imageSize = width * height * 4;

	if (!pixels) {
		throw std::runtime_error("failed to load texture: " + std::string(filename));
	}

	Buffer staggingBuffer;
	staggingBuffer.create(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	void* data;
	Device::MapMemory(staggingBuffer.getMemory(), 0, imageSize, &data);
	memcpy(data, pixels, static_cast<size_t>(imageSize));
	Device::UnmapMemory(staggingBuffer.getMemory());

	stbi_image_free(pixels);

	image.create(width, height, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	image.transitionLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	image.copyBuffer(staggingBuffer);
	image.transitionLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	staggingBuffer.cleanup();

	imageView = image.createImageView();
}

void Texture::create(glm::ivec4 color)
{
	int width = 1, height = 1;

	VkDeviceSize imageSize = width * height * 4;

	Buffer staggingBuffer;
	staggingBuffer.create(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	uint8_t c[4] = { color.r, color.g, color.b, color.a };
	void* data;
	Device::MapMemory(staggingBuffer.getMemory(), 0, imageSize, &data);
	memcpy(data, c, static_cast<size_t>(imageSize));
	Device::UnmapMemory(staggingBuffer.getMemory());

	image.create(width, height, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	image.transitionLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	image.copyBuffer(staggingBuffer);
	image.transitionLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	staggingBuffer.cleanup();

	imageView = image.createImageView();
}

void Texture::cleanup()
{
	Device::DestroyImageView(imageView);
	image.cleanup();
}

VkImageView Texture::getImageView() const
{
	return imageView;
}
