#pragma once
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Mesh.h"

class Model
{
public:
	Model(const std::string& path);
	~Model();

	uint32_t getMeshCount() const;
	void createDescriptorSets(VkDescriptorSetLayout layout, uint32_t count, VkSampler textureSampler);

	VkDeviceSize neededDeviceSize() const;
	uint32_t FillBuffer(Buffer& buffer, uint32_t offset);

	static Model* PostprocessQuad(const std::vector<VkImageView>& imageViews);
private:
	Model(Mesh singleMesh);

	friend class Renderer;
	friend class RenderPass;

	void cleanup();
	std::vector<Mesh> meshes;
	std::string directory;
	std::vector<ModelTexture> texturesLoaded;

	uint32_t bufferOffset;

	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	ModelTexture loadMaterialTexture(aiMaterial* mat, aiTextureType type);
};

