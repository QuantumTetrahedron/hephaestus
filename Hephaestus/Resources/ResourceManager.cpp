#include "ResourceManager.h"
#include "../Gfx/Renderer.h"

std::map<std::string, std::shared_ptr<Model>> ResourceManager::models;
std::map<std::string, std::shared_ptr<Texture>> ResourceManager::textures;
std::map<std::string, std::shared_ptr<Shader>> ResourceManager::shaders;

std::shared_ptr<Model> ResourceManager::GetModel(const std::string& name) {
    if (!HasModel(name)) {
        throw std::runtime_error("Model not found: " + name);
    }
    return models.at(name);
}

void ResourceManager::LoadModel(const char* filePath, const std::string& name) {
    auto it = models.find(name);
    if (it == models.end()) {
        auto model = new Model(filePath);
        models.emplace(name, model);
    }
}

bool ResourceManager::HasModel(const std::string& name) {
    return models.find(name) != models.end();
}

void ResourceManager::DeleteModel(const std::string& name) {
    auto it = models.find(name);
    if (it != models.end()) {
        models.erase(it);
    }
}

std::shared_ptr<Texture> ResourceManager::GetTexture(const std::string& name) {
    if (!HasTexture(name)) {
        throw std::runtime_error("Texture not found: " + name);
    }
    return textures.at(name);
}

void ResourceManager::LoadTexture(const char* filePath, const std::string& name) {
    auto it = textures.find(name);
    if (it == textures.end()) {
        auto texture = new Texture(filePath);
        textures.emplace(name, texture);
    }
}

bool ResourceManager::HasTexture(const std::string& name) {
    return textures.find(name) != textures.end();
}

void ResourceManager::DeleteTexture(const std::string& name) {
    auto it = textures.find(name);
    if (it != textures.end()) {
        textures.erase(it);
    }
}

std::shared_ptr<Shader> ResourceManager::GetShader(const std::string& name) {
    if (!HasShader(name)) {
        throw std::runtime_error("Shader not found: " + name);
    }
    return shaders.at(name);
}

void ResourceManager::LoadShader(const char* vertexPath, const char* fragmentPath, const std::string& name) {
    auto it = shaders.find(name);
    if (it == shaders.end()) {
        auto shader = new Shader(vertexPath, fragmentPath);
        shaders.emplace(name, shader);
    }
}

bool ResourceManager::HasShader(const std::string& name) {
    return shaders.find(name) != shaders.end();
}

void ResourceManager::DeleteShader(const std::string& name) {
    auto it = shaders.find(name);
    if (it != shaders.end()) {
        shaders.erase(it);
    }
}

void ResourceManager::Cleanup()
{
    models.clear();
    textures.clear();
    shaders.clear();

    Texture::clearEmpty();
}

void ResourceManager::LoadQuadModel(std::string name, const std::vector<VkImageView>& imageViews)
{
    auto it = models.find(name);
    if (it == models.end()) {
        auto model = Model::PostprocessQuad(imageViews);
        models.emplace(name, model);
    }
}
