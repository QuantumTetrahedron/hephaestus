#pragma once

#include <map>
#include <memory>
#include "Model.h"
#include "Shader.h"

class ResourceManager
{
public:
	static std::shared_ptr<Model> GetModel(const std::string& name);
	static void LoadModel(const char* filePath, const std::string& name);
	static bool HasModel(const std::string& name);
	static void DeleteModel(const std::string& name);

	static std::shared_ptr<Texture> GetTexture(const std::string& name);
	static void LoadTexture(const char* filePath, const std::string& name);
	static bool HasTexture(const std::string& name);
	static void DeleteTexture(const std::string& name);

	static std::shared_ptr<Shader> GetShader(const std::string& name);
	static void LoadShader(const char* vertexPath, const char* fragmentPath, const std::string& name);
	static bool HasShader(const std::string& name);
	static void DeleteShader(const std::string& name);

	static void Cleanup();

	static void LoadQuadModel(std::string name, const std::vector<VkImageView>& imageViews);
private:
	friend class Renderer;
	friend class RenderPass;
	static std::map<std::string, std::shared_ptr<Model>> models;
	static std::map<std::string, std::shared_ptr<Texture>> textures;
	static std::map<std::string, std::shared_ptr<Shader>> shaders;
};

