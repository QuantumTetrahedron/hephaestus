#pragma once
#include <vector>
#include <memory>

#include "Texture.h"
#include "../Gfx/DescriptorPool.h"
#include "../Gfx/GraphicsPipeline.h"
#include "../Gfx/Buffer.h"
#include "../Gfx/RenderTarget.h"
#include "Shader.h"

struct ModelTexture {
	std::shared_ptr<Texture> texture;
	std::string path;
};

class Mesh
{
public:
	Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices, const std::vector<ModelTexture>& texs);
	void cleanup();

	void Bind(uint32_t rpId, VkCommandBuffer command, Shader& shader, Buffer& buffer);
	
	void Draw(VkCommandBuffer command);
	//void DrawInstanced(VkCommandBuffer command, uint32_t instanceCount); todo

	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;
	std::vector<ModelTexture> textures;

	static DescriptorPool textureDescriptorPool;

	void createDescriptorSets(VkDescriptorSetLayout layout, uint32_t count, VkSampler textureSampler);

	VkDeviceSize neededDeviceSize() const;
	uint32_t FillBuffer(Buffer& buffer, uint32_t offset);

	static Mesh PostprocessQuad(const std::vector<VkImageView>& imageViews);
private:
	Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices, const std::vector<VkImageView>& _imageView);
	std::vector<VkImageView> imageViews;

	uint32_t bufferOffset;
	VkDescriptorSet textureDescriptorSet;
};

