#include "Mesh.h"
#include "../Gfx/Device.h"

DescriptorPool Mesh::textureDescriptorPool;

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices, const std::vector<ModelTexture>& texs)
	: vertices(vertices), indices(indices), textures(texs)
{
    for (auto tex : textures) {
        imageViews.push_back(tex.texture->getImageView());
    }
}

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices, const std::vector<VkImageView>& _imageViews) 
: vertices(vertices), indices(indices), imageViews(_imageViews) {
}

void Mesh::cleanup()
{
}

void Mesh::Bind(uint32_t rpId, VkCommandBuffer command, Shader& shader, Buffer& buffer)
{
    shader.BindDescriptors(rpId, command, 2, 1, &textureDescriptorSet);

    VkBuffer buffers[] = { buffer.get() };
    VkDeviceSize offsets[] = { bufferOffset };
    vkCmdBindVertexBuffers(command, 0, 1, buffers, offsets);

    VkDeviceSize offset = bufferOffset + sizeof(vertices[0]) * vertices.size();
    vkCmdBindIndexBuffer(command, buffer.get(), offset, VK_INDEX_TYPE_UINT32);
}

void Mesh::Draw(VkCommandBuffer command)
{
    vkCmdDrawIndexed(command, static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
}

/*void Mesh::DrawInstanced(VkCommandBuffer command, uint32_t instanceCount)
{
    vkCmdDrawIndexed(command, static_cast<uint32_t>(indices.size()), instanceCount, 0, 0, 0);
}*/

void Mesh::createDescriptorSets(VkDescriptorSetLayout layout, uint32_t count, VkSampler textureSampler)
{    
    std::vector<VkDescriptorSetLayout> layouts(1, layout);
    textureDescriptorPool.Allocate(1, layouts.data(), &textureDescriptorSet);

    std::vector<VkDescriptorImageInfo> imageInfos;

    for (size_t j = 0; j < count; ++j) {
        VkDescriptorImageInfo info{};
        if (j < imageViews.size()) {
            info.imageView = imageViews[j];
        }
        else {
            info.imageView = Texture::empty()->getImageView();
        }
        info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        info.sampler = textureSampler;
        imageInfos.push_back(info);
    }

    VkWriteDescriptorSet descriptorWrite{};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = textureDescriptorSet;
    descriptorWrite.dstBinding = 0;
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrite.descriptorCount = imageInfos.size();
    descriptorWrite.pImageInfo = imageInfos.data();

    Device::UpdateDescriptorSets(1, &descriptorWrite, 0, nullptr);
}

VkDeviceSize Mesh::neededDeviceSize() const
{
    return sizeof(vertices[0]) * vertices.size() + sizeof(indices[0]) * indices.size();
}

uint32_t Mesh::FillBuffer(Buffer& buffer, uint32_t offset)
{
    bufferOffset = offset;
    VkDeviceSize vertexSize = sizeof(vertices[0]) * vertices.size();
    VkDeviceSize indexSize = sizeof(indices[0]) * indices.size();

    void* vertexData;
    void* indexData;
    Device::MapMemory(buffer.getMemory(), offset, vertexSize, &vertexData);
    memcpy(vertexData, vertices.data(), (size_t)vertexSize);
    Device::UnmapMemory(buffer.getMemory());

    Device::MapMemory(buffer.getMemory(), offset + vertexSize, indexSize, &indexData);
    memcpy(indexData, indices.data(), (size_t)indexSize);
    Device::UnmapMemory(buffer.getMemory());

    return offset + vertexSize + indexSize;
}

Mesh Mesh::PostprocessQuad(const std::vector<VkImageView>& imageViews)
{
    std::vector<Vertex> vertices(4);

    vertices[0].position = { -1.f, -1.f, 0.0f };
    vertices[1].position = { 1.f, -1.f, 0.0f };
    vertices[2].position = { 1.f, 1.f, 0.0f };
    vertices[3].position = { -1.f, 1.f, 0.0f };

    vertices[0].normal = { 0.0f, 0.0f, 1.0f };
    vertices[1].normal = { 0.0f, 0.0f, 1.0f };
    vertices[2].normal = { 0.0f, 0.0f, 1.0f };
    vertices[3].normal = { 0.0f, 0.0f, 1.0f };

    vertices[0].texCoord = { 0.0f, 0.0f };
    vertices[1].texCoord = { 1.0f, 0.0f };
    vertices[2].texCoord = { 1.0f, 1.0f };
    vertices[3].texCoord = { 0.0f, 1.0f };

    std::vector<uint32_t> indices = {
        0, 1, 2, 0, 2, 3
    };

    return Mesh(vertices, indices, imageViews);
}

