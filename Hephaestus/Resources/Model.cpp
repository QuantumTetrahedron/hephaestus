#include "Model.h"
#include <stdexcept>

Model::Model(const std::string& path)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		throw std::runtime_error("failed to load model");
	}

	directory = path.substr(0, path.find_last_of('/'));
	processNode(scene->mRootNode, scene);
}

Model::~Model() {
	cleanup();
}

void Model::cleanup()
{
	for (auto& mesh : meshes) {
		mesh.cleanup();
	}
}

uint32_t Model::getMeshCount() const
{
	return static_cast<uint32_t>(meshes.size());
}

void Model::createDescriptorSets(VkDescriptorSetLayout layout, uint32_t count, VkSampler textureSampler)
{
	for (auto& mesh : meshes) {
		mesh.createDescriptorSets(layout, count, textureSampler);
	}
}

VkDeviceSize Model::neededDeviceSize() const
{
	VkDeviceSize totalSize = 0;
	for (auto& m : meshes) {
		totalSize += m.neededDeviceSize();
	}
	return totalSize;
}

uint32_t Model::FillBuffer(Buffer& buffer, uint32_t offset) {
	bufferOffset = offset;

	uint32_t currentOffset = offset;
	for (auto& mesh : meshes) {
		currentOffset = mesh.FillBuffer(buffer, currentOffset);
	}
	return currentOffset;
}

Model* Model::PostprocessQuad(const std::vector<VkImageView>& imageViews)
{
	Mesh mesh = Mesh::PostprocessQuad(imageViews);
	return new Model(mesh);
}

Model::Model(Mesh singleMesh) {
	meshes.push_back(singleMesh);
}

void Model::processNode(aiNode* node, const aiScene* scene)
{
	for (unsigned int i = 0; i < node->mNumMeshes; ++i) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(mesh, scene));
	}
	for (unsigned int i = 0; i < node->mNumChildren; ++i) {
		processNode(node->mChildren[i], scene);
	}
}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;
	ModelTexture diffuse;

	for (unsigned int i = 0; i < mesh->mNumVertices; ++i) {
		Vertex vertex;

		glm::vec3 v;
		v.x = mesh->mVertices[i].x;
		v.y = mesh->mVertices[i].y;
		v.z = mesh->mVertices[i].z;
		vertex.position = v;
		
		v.x = mesh->mNormals[i].x;
		v.y = mesh->mNormals[i].y;
		v.z = mesh->mNormals[i].z;
		vertex.normal = v;

		if (mesh->mTextureCoords[0]) {
			vertex.texCoord = glm::vec2(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
		}
		else {
			vertex.texCoord = glm::vec2(0.0f);
		}

		vertices.push_back(vertex);
	}
	
	for (unsigned int i = 0; i < mesh->mNumFaces; ++i) {
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; ++j) {
			indices.push_back(face.mIndices[j]);
		}
	}

	if (mesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		diffuse = loadMaterialTexture(material, aiTextureType_DIFFUSE);
	}

	std::vector<ModelTexture> vec{ diffuse };

	return Mesh(vertices, indices, vec);
}

ModelTexture Model::loadMaterialTexture(aiMaterial* mat, aiTextureType type)
{
	for (unsigned int i = 0; i < mat->GetTextureCount(type); ++i) {
		aiString str;
		mat->GetTexture(type, i, &str);

		for (auto& tex : texturesLoaded) {
			if (std::strcmp(tex.path.data(), str.C_Str()) == 0) {
				return tex;
			}
		}
		
		ModelTexture texture;
		texture.path = str.C_Str();
		std::string p = directory + "/" + std::string(str.C_Str());
		texture.texture = std::make_shared<Texture>(p.c_str());
		texturesLoaded.push_back(texture);
		return texture;
	}
	throw std::runtime_error("failed to find model texture of a given type");
}

