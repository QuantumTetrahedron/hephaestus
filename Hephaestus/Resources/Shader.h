#pragma once
#include "../Gfx/GraphicsPipeline.h"

class Shader
{
public:
	// Change to read all from an .ini file
	Shader(const std::string& vertexShaderPath, const std::string& fragmentShaderPath);
	~Shader();

	void Init(uint32_t rpId, const RenderPass& renderPass, VkExtent2D extent);

	void Use(uint32_t rpId, VkCommandBuffer command);
	void BindDescriptors(uint32_t rpId, VkCommandBuffer command, uint32_t first_set, uint32_t set_count, const VkDescriptorSet* descriptorSets);

private:
	std::map<uint32_t, GraphicsPipeline> pipelines;

	std::string vertexPath, fragmentPath;
};

