#include "Shader.h"

Shader::Shader(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
	vertexPath = vertexShaderPath;
	fragmentPath = fragmentShaderPath;
}

Shader::~Shader()
{
	for (auto& p : pipelines) {
		p.second.cleanup();
	}
}

void Shader::Init(uint32_t rpId, const RenderPass& renderPass, VkExtent2D extent)
{
	if (pipelines.find(rpId) != pipelines.end()) {
		pipelines[rpId].cleanup();
		pipelines.erase(rpId);
	}
	pipelines[rpId].create(vertexPath, fragmentPath, renderPass, extent);
}

void Shader::Use(uint32_t rpId, VkCommandBuffer command)
{
	vkCmdBindPipeline(command, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.at(rpId).get());
}

void Shader::BindDescriptors(uint32_t rpId, VkCommandBuffer command, uint32_t first_set, uint32_t set_count, const VkDescriptorSet* descriptorSets)
{
	vkCmdBindDescriptorSets(command, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.at(rpId).getLayout(), first_set, set_count, descriptorSets, 0, nullptr);
}
