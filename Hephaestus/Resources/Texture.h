#pragma once
#include "../Gfx/Image.h"
#include "../Gfx/Framebuffer.h"

class Texture
{
public:
	Texture(const char* filename);
	~Texture();

	VkImageView getImageView() const;
	Image image;

	static std::shared_ptr<Texture> empty() {
		if (!_hasEmpty) {
			_empty = std::shared_ptr<Texture>(new Texture());
			_empty->create(glm::ivec4(0));
			_hasEmpty = true;
		}

		return _empty;
	}

	static void clearEmpty() {
		if (_hasEmpty) {
			_empty = nullptr;
		}
	}
private:
	Texture() {}

	static std::shared_ptr<Texture> _empty;
	static bool _hasEmpty;

	void create(const char* filename);
	void create(glm::ivec4 color);

	void cleanup();

	VkImageView imageView;
};

