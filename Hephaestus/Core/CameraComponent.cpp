#include "CameraComponent.h"
#include <glm/gtc/matrix_transform.hpp>
#include "../Gfx/Renderer.h"

CameraComponent::CameraComponent()
    : worldUp(0.0f, 1.0f, 0.0f),
    isMain(false),
    drawWorldSpace(true),
    drawHUDSpace(true),
    screenWidth(0),
    screenHeight(0),
    front(1.0f, 0.0f, 0.0f),
    fieldOfView(0.0f),
    aspectRatio(0.0f) {}

glm::mat4 CameraComponent::GetViewMatrix() const {
    glm::vec3 position = GetParent()->transform.position;
    return glm::lookAt(position, position + front, worldUp);
}

glm::mat4 CameraComponent::GetProjectionMatrix() const {
    glm::mat4 proj = glm::perspective(glm::radians(fieldOfView), aspectRatio, 0.1f, 100.0f);
    proj[1][1] *= -1;
    return proj;
}

bool CameraComponent::LoadFromFile(const IniFile& file) {
    file.RequireValue("fieldOfView", fieldOfView);

    VkExtent2D extent = Renderer::getSwapchainExtent();
    screenWidth = extent.width;
    screenHeight = extent.height;

    aspectRatio = (float)screenWidth / (float)screenHeight;

    file.GetValue("isMain", isMain, false);

    file.GetValue("drawWorldSpace", drawWorldSpace, true);
    file.GetValue("drawHUDSpace", drawHUDSpace, true);

    file.GetValue("front", front, glm::vec3(1.0f, 0.0f, 0.0f));

    return true;
}

void CameraComponent::OnLoad() {
    if (isMain) {
        Activate();
    }
}

glm::vec3 CameraComponent::GetPosition() const {
    return GetParent()->transform.position;
}

int CameraComponent::GetWidth() const {
    return screenWidth;
}

int CameraComponent::GetHeight() const {
    return screenHeight;
}

glm::vec3 CameraComponent::GetFront() const {
    return front;
}

glm::mat4 CameraComponent::GetHUDProjectionMatrix() const {
    return glm::ortho(0.0f, (float)screenWidth, (float)screenHeight, 0.0f, -100.0f, 100.0f);
}

void CameraComponent::SetResolution(int w, int h) {
    screenWidth = w;
    screenHeight = h;
}

void CameraComponent::SetFront(glm::vec3 newFront) {
    front = newFront;
}

void CameraComponent::Activate() {
    Renderer::ActivateCamera(this);
}