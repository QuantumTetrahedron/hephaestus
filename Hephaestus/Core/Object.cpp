#include "Object.h"
#include "../Resources/Model.h"
#include "../Gfx/Device.h"
#include "../Gfx/Buffer.h"
#include "../Gfx/GraphicsPipeline.h"

#include <queue>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Component.h"
#include "BehaviourComponent.h"

std::shared_ptr<Object> Object::create() {
    return std::shared_ptr<Object>(new Object);
}

Object::Object()
    : isDead(false), isActive(true) {
    transform.position = glm::vec3(0.0f);
    transform.rotation = glm::vec3(0.0f);
    transform.scale = glm::vec3(1.0f);
    transform.pivot = glm::vec3(0.0f);
}

void Object::Update(float dt) {
    for (const auto& bc : behaviours) {
        bc->Update(dt);
    }

    std::queue<std::shared_ptr<Component>> toDelete;
    for (const auto& c : components)
    {
        if (c->isDead)
            toDelete.push(c);
    }
    while (!toDelete.empty()) {
        std::shared_ptr<Component> c = toDelete.front();
        toDelete.pop();
        RemoveComponent(c);
    }
}

void Object::AddComponent(const std::shared_ptr<Component>& c) {
    if (std::find(components.begin(), components.end(), c) != components.end()) {
        std::cerr << "Component already added" << std::endl;
        return;
    }
    c->SetParent(shared_from_this());
    components.push_back(c);

    auto bc = std::dynamic_pointer_cast<BehaviourComponent>(c);
    if (bc) {
        behaviours.push_back(bc);
    }
}

void Object::RemoveComponent(const std::shared_ptr<Component>& c) {
    auto i = std::find(components.begin(), components.end(), c);
    if (i == components.end()) {
        std::cerr << "Component not found" << std::endl;
        return;
    }
    std::iter_swap(i, --components.end());
    components.pop_back();

    auto bc = std::dynamic_pointer_cast<BehaviourComponent>(c);
    if (bc) {
        auto bi = std::find(behaviours.begin(), behaviours.end(), bc);
        if (bi != behaviours.end()) {
            std::iter_swap(bi, --behaviours.end());
            behaviours.pop_back();
        }
    }

    c->SetParent(nullptr);
    c->OnRemove();
}

void Object::RemoveAllComponents() {
    for (const auto& c : components) {
        c->SetParent(nullptr);
        c->OnRemove();
    }
    components.clear();
    behaviours.clear();
}

void Object::LoadFromFile(const IniFile& file) {
    file.GetValue("position", transform.position, glm::vec3(0.0f));
    file.GetValue("rotation", transform.rotation, glm::vec3(0.0f));
    file.GetValue("scale", transform.scale, glm::vec3(1.0f));
    file.GetValue("pivot", transform.pivot, glm::vec3(0.0f));
}

std::shared_ptr<Object> Object::Clone() const {
    auto ret = Object::create();
    ret->components.clear();
    for (const auto& c : components) {
        auto comp_clone = c->Clone();
        ret->AddComponent(comp_clone);
    }
    for (const auto& c : ret->components) {
        c->OnClone();
    }
    ret->transform = transform;
    return ret;
}

void Object::SetParent(const std::shared_ptr<Object>& o) {
    parent = o;
}

Transform Object::GetWorldTransform() const {
    auto p = GetParent();
    if (p) {
        Transform parentTransform = p->GetWorldTransform();
        glm::mat4 model(1.0f);
        model = glm::rotate(model, glm::radians(parentTransform.rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(parentTransform.rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::rotate(model, glm::radians(parentTransform.rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(parentTransform.scale));
        Transform ret{
            glm::vec3(model * glm::vec4(transform.position - parentTransform.pivot, 1.0f)) + parentTransform.position,
            parentTransform.rotation + transform.rotation,
            parentTransform.scale * transform.scale,
            transform.pivot
        };
        return ret;
    }
    else return transform;
}

bool Object::HasParent() const {
    return !parent.expired();
}

bool Object::IsDead() const {
    return isDead;
}

void Object::OnDestroy() {
    for (const auto& bc : behaviours)
        bc->OnLeave();
    RemoveAllComponents();
}

void Object::Start() {
    for (const auto& bc : behaviours)
        bc->Start();
}

bool Object::IsActive() const {
    auto p = GetParent();
    if (p)
        return isActive && p->IsActive();
    else
        return isActive;
}

void Object::SetActive(bool active) {
    bool wasActive = isActive;
    isActive = active;
    if (!wasActive && IsActive()) {
        OnActive();
    }
}

void Object::OnActive() {
    for (const auto& c : components) {
        if (c->IsActive()) {
            c->OnActive();
        }
    }
}

std::string Object::GetName() const {
    return name;
}

void Object::Kill() {
    isDead = true;
}

std::shared_ptr<Object> Object::GetParent() const {
    return parent.lock();
}