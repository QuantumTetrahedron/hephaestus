#pragma once

#include <map>
#include <memory>
#include <string>

class Component;

template<typename T>
std::shared_ptr<Component> createT() {
    return std::shared_ptr<T>(new T);
}

class ComponentFactory
{
public:
    typedef std::map<std::string, std::shared_ptr<Component>(*)()> mapType;
    static std::shared_ptr<Component> create(const std::string& className);

    template<typename T>
    static std::shared_ptr<T> clone(std::shared_ptr<const Component> component);

protected:
    static std::shared_ptr<mapType> getMap() noexcept;
private:
    static std::shared_ptr<mapType> map;
};

template<typename T>
class ComponentRegister : public ComponentFactory {
public:
    explicit ComponentRegister(const std::string& className) noexcept {
        try {
            auto p = std::make_pair(className, &createT<T>);
            getMap()->insert(p);
        }
        catch (...) {}
    }
};

template<typename T>
std::shared_ptr<T> ComponentFactory::clone(std::shared_ptr<const Component> component) {
    std::shared_ptr<const T> comp = std::dynamic_pointer_cast<const T>(component);
    auto c = std::make_shared<T>(*comp);
    return c;
}
