#include "Component.h"

Component::Component() : isDead(false), isActive(true) {
    parent.reset();
}

void Component::SetParent(const std::shared_ptr<Object>& p) {
    parent = p;
}

std::shared_ptr<const Object> Component::GetParent() const {
    return parent.lock();
}

bool Component::IsDead() const {
    return isDead;
}

void Component::Kill() {
    isDead = true;
}

bool Component::IsActive() const {
    if (!parent.expired())
        return isActive && GetParent()->IsActive();
    else
        return isActive;
}

void Component::SetActive(bool active) {
    bool wasActive = isActive;
    isActive = active;
    if (!wasActive && IsActive()) {
        OnActive();
    }
}

bool Component::LoadFromFile(const IniFile& file) {
    return true;
}

void Component::OnClone() {
    parent.reset();
}