#pragma once

#include "Component.h"

class CameraComponent : public Component {
public:
    CameraComponent();

    [[nodiscard]] glm::vec3 GetFront() const;
    [[nodiscard]] int GetWidth() const;
    [[nodiscard]] int GetHeight() const;

    glm::vec3 GetPosition() const;
    glm::mat4 GetViewMatrix() const;
    glm::mat4 GetProjectionMatrix() const;
    glm::mat4 GetHUDProjectionMatrix() const;

    void SetResolution(int w, int h);
    void SetFront(glm::vec3 newFront);

    bool LoadFromFile(const IniFile& file) override;
    void OnLoad() override;

    void Activate();

    const glm::vec3 worldUp;

    bool isMain;
    bool drawWorldSpace, drawHUDSpace;

    float fieldOfView;
    int screenWidth;
    int screenHeight;
    float aspectRatio;

    glm::vec3 front;

    COMPONENT(CameraComponent)
};