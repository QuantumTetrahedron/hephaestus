#pragma once

#include "Component.h"

class Buffer;
class Model;
class Shader;
struct ObjectUniforms;

class RenderComponent : public Component
{
public:
	RenderComponent();

	void SetModel(const std::string& newModelName);
	void SetModel(std::shared_ptr<Model> newModel);
	std::shared_ptr<Model> GetModel() const;

	virtual void UpdateUniforms(ObjectUniforms* uniforms) const;
protected:
	friend class Renderer;
	friend class RenderPass;

	std::shared_ptr<Model> model;
	std::shared_ptr<Shader> shader;
	std::string groupName;
	
	bool LoadFromFile(const IniFile& file) override;
	void OnLoad() override;
	void OnClone() override;
	void OnRemove() override;

	size_t descriptorSetIndex;

	COMPONENT(RenderComponent);
};

