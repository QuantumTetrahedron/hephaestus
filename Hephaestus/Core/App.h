#pragma once
#include <vector>
#include "../Gfx/Instance.h"
#include "../Gfx/Renderer.h"
#include "../Gfx/Surface.h"
#include "../Input/Window.h"
#include "AppInfo.h"

class App
{
public:
	void Initialize(const AppInfo& info);
	void MainLoop();
	void Cleanup();

	int getFPS();
private:
	void Start();

	void LoadWindow(const AppInfo& info);

	std::vector<const char*> getRequiredExtensions();

	Instance instance;
	Window window;
	Surface surface;

	std::string initialVortexPath;
	float fps, lastFPS, deltaTime, lastFrameTime;

#ifdef NDEBUG
	const bool debug = false;
#else
	const bool debug = true;
#endif
};
