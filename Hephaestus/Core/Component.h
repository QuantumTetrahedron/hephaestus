#pragma once

#include "ComponentFactory.h"
#include "Object.h"

class Component : public std::enable_shared_from_this<Component>
{
public:
	virtual ~Component() = default;
	virtual std::shared_ptr<Component> Clone() const = 0;
	
	bool IsDead() const;
	void Kill();
	bool IsActive() const;
	void SetActive(bool active);
	std::shared_ptr<const Object> GetParent() const;

	friend class Object;
	friend class ObjectFactory;
protected:
	Component();

	virtual bool LoadFromFile(const IniFile& file);
	
	virtual void OnLoad() {}
	virtual void OnActive() {}
	virtual void OnClone();
	virtual void OnRemove() {}

	std::weak_ptr<Object> parent;
	bool isDead;
	bool isActive;

	void SetParent(const std::shared_ptr<Object>& p);
};

#define COMPONENT(compType) \
    protected: \
        std::shared_ptr<Component> Clone() const override { \
            return ComponentFactory::clone<compType>(shared_from_this()); \
        } \
    private: \
        inline static const ComponentRegister<compType> reg{#compType};
