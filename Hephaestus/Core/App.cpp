#include "App.h"
#include "../Gfx/Device.h"
#include "../Gfx/CommandPool.h"
#include "../Resources/ResourceManager.h"
#include "SceneManager.h"
#include "../Input/Input.h"

void App::Initialize(const AppInfo& info) {
    LoadWindow(info);
    initialVortexPath = info.vortexPath;

    auto extensions = getRequiredExtensions();
    instance.create(extensions, debug);
    surface.create(instance, window);

    Device::create(instance, surface);
    CommandPool::create();

    Input::Initialize(&window);

    SceneManager::Init();
    SceneManager::SetStartingScene(info.startScene);
}

void App::MainLoop() {
    Start();
    static float time = 0.0f;
    while (!window.shouldClose()) {
        auto currentFrameTime = (float)glfwGetTime();
        deltaTime = currentFrameTime - lastFrameTime;
        lastFrameTime = currentFrameTime;
        time += deltaTime;
        fps++;
        if (time >= 1.0f) {
            time -= 1.0f;
            lastFPS = fps;
            fps = 0;
            std::cout << "fps " << lastFPS << std::endl;
        }
        //else continue;
        //deltaTime = 0.5;

        glfwPollEvents();

        if (window.hasResized()) {
            while (window.width == 0 || window.height == 0) {
                glfwWaitEvents();
                int w, h;
                glfwGetFramebufferSize(window.get(), &w, &h);
                window.width = w;
                window.height = h;
                if (window.shouldClose()) return;
            }

            surface.width = window.width;
            surface.height = window.height;
            Renderer::OnResize();
        }

        SceneManager::Update(deltaTime);
        Renderer::Render(surface);
        
        SceneManager::ProcessRequest();

        Input::Reset();
    }
}

void App::Start() {
    Renderer::Initialize(surface, initialVortexPath);
    SceneManager::Start();
}

void App::LoadWindow(const AppInfo& info)
{
    window.create(info.windowWidth, info.windowHeight, info.title.c_str());

    window.SetKeyCallback([](const Window& window, int key, int scancode, int action, int mods) {
        Input::ProcessKeyInput(key, action);
    });

    window.SetCursorPosCallback([](const Window& window, double xPos, double yPos) {
        Input::ProcessMouseMove((float)xPos, (float)yPos);
    });

    window.SetMouseButtonCallback([](const Window& window, int button, int action, int mods) {
        Input::ProcessMouseButton(button, action);
    });
}

void App::Cleanup() {
    SceneManager::CleanupAll();
    Renderer::Cleanup();
    CommandPool::cleanup();
    Device::cleanup();
    surface.cleanup(instance);
    instance.cleanup();
    window.cleanup();
}

std::vector<const char*> App::getRequiredExtensions() {
    uint32_t glfwExtensionsCount = 0;
    const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);
    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionsCount);

    if (debug) {
        extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    return extensions;
}

int App::getFPS() {
    return (int) lastFPS;
}