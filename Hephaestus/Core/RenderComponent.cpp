#include "RenderComponent.h"
#include "../Resources/ResourceManager.h"
#include "../Gfx/Buffer.h"
#include "../Gfx/Device.h"
#include "../Gfx/Renderer.h"

bool RenderComponent::LoadFromFile(const IniFile& file) {
    std::string modelName, shaderName;

    file.RequireValue("model", modelName);
    model = ResourceManager::GetModel(modelName);

    file.RequireValue("shader", shaderName);
    shader = ResourceManager::GetShader(shaderName);

    file.RequireValue("group", groupName);

    return true;
}

void RenderComponent::UpdateUniforms(ObjectUniforms* uniforms) const
{
    uniforms->model = GetParent()->GetWorldTransform().getModelMatrix();
}

RenderComponent::RenderComponent(){}

void RenderComponent::SetModel(const std::string& newModelName) {
    model = ResourceManager::GetModel(newModelName);
}

void RenderComponent::SetModel(std::shared_ptr<Model> m) {
    model = std::move(m);
}

std::shared_ptr<Model> RenderComponent::GetModel() const {
    return model;
}

void RenderComponent::OnLoad() {
    Renderer::RegisterComponent(std::dynamic_pointer_cast<RenderComponent>(shared_from_this()));
}

void RenderComponent::OnClone() {
    Component::OnClone();
    Renderer::RegisterComponent(std::dynamic_pointer_cast<RenderComponent>(shared_from_this()));
}

void RenderComponent::OnRemove() {
    Renderer::UnregisterComponent(std::dynamic_pointer_cast<RenderComponent>(shared_from_this()));
}