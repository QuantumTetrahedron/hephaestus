#pragma once

#include <memory>
#include "../Utils/PausableVector.h"
#include "Object.h"

class ObjectManager
{
public:
	static void AddObject(std::shared_ptr<Object> o);
	static std::shared_ptr<Object> GetObject(const std::string& name);
	static void DestroyObjects();

	static std::vector<std::shared_ptr<Object>> GetChildrenOf(const Object& obj);
	static std::vector<std::shared_ptr<Object>> GetChildrenOf(const std::string& objName);
private:
	friend class SceneManager;
	static PausableVector<std::shared_ptr<Object>> objects;

	static void Init();
	static void Start();
	static void Update(float dt);
	static void Pause();
	static void Resume();
	static void DestroyAllObjects();
};

