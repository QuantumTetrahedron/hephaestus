#include "RenderTarget.h"
#include "Device.h"
#include "Renderer.h"

void RenderTarget::create(const std::vector<ImageData>& imagesData, bool hasDepth, VkExtent2D ext)
{
	extent = ext;
	//formats = _formats;

	for (const ImageData& data : imagesData) {
		std::shared_ptr<Image> image = Renderer::getImage(data.name);
		colorImages.push_back(image);
		VkImageView imageView = image->createImageView();
		colorImageViews.push_back(imageView);
		//formats.push_back(data.format.has_value() ? data.format.value())
	}

	if (hasDepth) {
		depthImage = Renderer::getDepthImage();
		depthImageView = depthImage.value().lock()->createImageView();
	}
}

void RenderTarget::cleanup()
{
	for (auto& p : framebuffers) {
		p.second.destroy();
	}

	for (VkImageView view : colorImageViews) {
		Device::DestroyImageView(view);
	}

	if (depthImageView.has_value()) {
		Device::DestroyImageView(depthImageView.value());
	}
}

VkExtent2D RenderTarget::getExtent() const
{
	return extent;
}

/*
VkFormat RenderTarget::getImageFormat() const
{
	return format;
}*/


Framebuffer RenderTarget::getFramebuffer(uint32_t rpId) const
{
	return framebuffers.at(rpId);
}

/*
VkImageView RenderTarget::getImageView() const
{
	return imageView;
}*/

void RenderTarget::createFramebuffer(uint32_t rpId, const RenderPass& renderPass)
{
	if (framebuffers.find(rpId) == framebuffers.end()) {
		framebuffers[rpId].create(colorImageViews, depthImageView, renderPass, extent);
		//framebuffers[rpId].create(imageView, depthImageView, renderPass, extent, format);
	}
}

void RenderTarget::transitionLayout(VkImageLayout newLayout)
{
	for (int i = 0; i < colorImages.size(); ++i) {
		colorImages[i].lock()->transitionLayout(newLayout);
	}
}
