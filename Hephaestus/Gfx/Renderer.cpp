#include "Renderer.h"
#include "CommandPool.h"
#include "../Resources/ResourceManager.h"
#include "../Core/Object.h"
#include "Device.h"
#include "VortexGraph.h"

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

VortexGraph Renderer::graph;
std::map<uint32_t, RenderPass> Renderer::renderPasses;

CameraComponent* Renderer::activeCamera = nullptr;
PausableVector<std::shared_ptr<RenderComponent>> Renderer::renderComponents;
bool Renderer::framebufferResized = false;
DescriptorPool Renderer::globalDescriptorPool;
DescriptorPool Renderer::objectDescriptorPool;
Swapchain Renderer::swapchain;
Buffer Renderer::vertexIndexBuffer;
Buffer Renderer::uniformBuffer;
VkSampler Renderer::textureSampler;
std::vector<VkCommandBuffer> Renderer::commands;
VkDescriptorSet Renderer::globalDescriptorSet;
std::vector<VkDescriptorSet> Renderer::objectDescriptorSets;
bool Renderer::needsToRecreateBuffers = false;
bool Renderer::needsToReset = false;
VkDescriptorSetLayout Renderer::globalSetLayout;
VkDescriptorSetLayout Renderer::objectSetLayout;
VkDescriptorSetLayout Renderer::meshSetLayout;
std::string Renderer::requestedVortexGraphPath = "";
bool Renderer::needsToLoadGraph = false;

std::map<std::string, VarData<Renderer::variant>> Renderer::vars;
std::map<std::string, std::shared_ptr<Image>> Renderer::images;
std::shared_ptr<Image> Renderer::depthImage;
std::map<uint32_t, RenderTarget> Renderer::targets;

void Renderer::LoadGraph(const std::string& vortexPath)
{
    graph.Cleanup();
    requestedVortexGraphPath = vortexPath;
    needsToLoadGraph = true;
}

void Renderer::CleanupGraph()
{
    vars.clear();
    renderPasses.clear();
    needsToLoadGraph = true;
    requestedVortexGraphPath = "";
}

void Renderer::Initialize(const Surface& surface, const std::string& vortexPath)
{   
    swapchain.create(surface);

    LoadGraph(vortexPath);

    ImageData swapchainImageData = swapchain.getImageData();
    CreateImage(swapchainImageData);

    createDescriptorSetLayouts();
    createTextureSampler();
}

void Renderer::Render(const Surface& surface)
{
    if (needsToLoadGraph) {
        needsToLoadGraph = false;
        if (requestedVortexGraphPath == "") {
            throw std::runtime_error("No Vortex Graph Loaded");
        }

        if (!graph.LoadFromFile(requestedVortexGraphPath.c_str())) {
            throw std::runtime_error("Failed to open vortex graph file");
        }

        for (auto& target : targets) {
            target.second.transitionLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        }

        graph.Initialize();

        needsToRecreateBuffers = true;
    }

    if (needsToReset) {
        needsToReset = false;
        Reset();
        needsToRecreateBuffers = true;
    }

    if (needsToRecreateBuffers) {
        needsToRecreateBuffers = false;
        DestroyBuffersAndCommands();
        CreateBuffersAndCommands();
    }
    
    uint32_t imageIndex;
    VkResult result = swapchain.acquireImage(&imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        //device.recreateSwapchain();
        //return;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        throw std::runtime_error("failed to acquire swap chain image!");
    }

    updateUniforms();

    swapchain.submitCommands(&commands[imageIndex], 1);

    swapchain.present(imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized) {
        Resize(surface);
    }
    else if (result != VK_SUCCESS) {
        throw std::runtime_error("failed to present swap chain image!");
    }
}

void Renderer::RegisterComponent(std::shared_ptr<RenderComponent> rc) {
    renderComponents.push_back(std::move(rc));
    needsToRecreateBuffers = true;
}

void Renderer::UnregisterComponent(const std::shared_ptr<RenderComponent>& rc) {
    renderComponents.erase(std::remove_if(renderComponents.begin(), renderComponents.end(), [rc](auto r) {return r == rc; }), renderComponents.end());
    needsToRecreateBuffers = true;
}

void Renderer::Cleanup()
{
    Device::WaitIdle();

    uniformBuffer.cleanup();
    vertexIndexBuffer.cleanup();
    globalDescriptorPool.cleanup();
    
    ClearData();
    ResourceManager::Cleanup();
    
    objectDescriptorPool.cleanup();
    Mesh::textureDescriptorPool.cleanup();

    Device::DestroyDescriptorSetLayout(globalSetLayout);
    Device::DestroyDescriptorSetLayout(objectSetLayout);
    Device::DestroyDescriptorSetLayout(meshSetLayout);


    graph.Cleanup();

    for (auto& p : targets) {
        p.second.cleanup();
    }

    for (auto& i : images) {
        i.second->cleanup();
    }
    depthImage->cleanup();

    swapchain.cleanup();

    Device::DestroySampler(textureSampler);
}

void Renderer::ActivateCamera(CameraComponent* camera)
{
    activeCamera = camera;
}

void Renderer::OnResize() {
    framebufferResized = true;
}

std::vector<VkDescriptorSetLayout> Renderer::GetDescriptorSetLayouts()
{
    return { globalSetLayout, objectSetLayout, meshSetLayout };
}

void Renderer::Resize(const Surface& surface)
{
    framebufferResized = false;

    DestroyBuffersAndCommands();
    swapchain.recreate(surface);
    CreateBuffersAndCommands();
}

void Renderer::createBuffers() {
    VkDeviceSize vertexIndexBufferSize = 0;
    for (auto& p : ResourceManager::models) {
        vertexIndexBufferSize += p.second->neededDeviceSize();
    }

    if (vertexIndexBufferSize > 0) {
        Buffer stagingBuffer;
        stagingBuffer.create(vertexIndexBufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        uint32_t currentOffset = 0;
        for (auto& p : ResourceManager::models) {
            currentOffset = p.second->FillBuffer(stagingBuffer, currentOffset);
        }
        vertexIndexBuffer.create(vertexIndexBufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        vertexIndexBuffer.copy(stagingBuffer, vertexIndexBufferSize);
        stagingBuffer.cleanup();
    }

    VkDeviceSize uniformBufferSize = getGlobalUniformsSize();
    uniformBufferSize += sizeof(ObjectUniforms) * renderComponents.Size();

    if (uniformBufferSize > 0) {
        uniformBuffer.create(uniformBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    }
}

void Renderer::CreateRenderPass(uint32_t id, std::vector<ImageData>& imagesData)
{
    // create all renderpasses, passing graph data
    // * used shaders (1 shader in minimal version)
    // * components per shader (components to draw with the only shader)
    // * image output or swapchain

    CreateRenderTarget(id, imagesData);

    RenderPass renderPass;
    RenderPassData data;

    if (!GetVar<RenderPassData>(std::to_string(id), &data)) {
        std::cerr << "No render pass data found for id: " << id << std::endl;
        return;
    }

    std::vector<RenderPass::Config> configs;
    for (int i = 0; i < imagesData.size(); ++i) {
        RenderPass::Config config = renderPass.createConfig(imagesData[i].format.value_or(swapchain.getImageFormat()),
            data.clearColor, false, imagesData[i].isSwapchain);
        configs.push_back(config);
    }

    std::optional<RenderPass::Config> depthConfig;
    if (data.depthTest) {
        depthConfig = renderPass.createConfig(Device::findDepthFormat(), data.clearDepth, true, false);
    }

    renderPass.create(id, configs, depthConfig, ResourceManager::GetShader(data.shaderName));

    // render pass should own its shaders
    // pipelines are per render pass
    ResourceManager::GetShader(data.shaderName)->Init(id, renderPass, swapchain.getExtent());

    renderPasses[id] = renderPass;
}

void Renderer::CleanupRenderPass(uint32_t id)
{
    DestroyRenderTarget(id);
    renderPasses[id].cleanup();
    renderPasses.erase(id);
}

VkExtent2D Renderer::getSwapchainExtent()
{
    return swapchain.getExtent();
}

std::shared_ptr<Image> Renderer::getImage(std::string name)
{
    return images.at(name);
}

void Renderer::CreateImage(ImageData& imageData)
{
    auto image = std::make_shared<Image>();
    VkExtent2D ext = imageData.extent.value_or(swapchain.getExtent());
    VkFormat format = imageData.format.value_or(swapchain.getImageFormat());

    image->create(ext.width, ext.height, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    images[imageData.name] = image;
}

std::shared_ptr<Image> Renderer::getDepthImage()
{
    if (!depthImage) {
        depthImage = std::make_shared<Image>();
        VkExtent2D ext = swapchain.getExtent();
        VkFormat depthFormat = Device::findDepthFormat();
        depthImage->create(ext.width, ext.height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);
    }

    return depthImage;
}

void Renderer::Reset() {
    ResetVars();
    Device::WaitIdle();
    graph.Reset();
}

void Renderer::ResetVars()
{
    for (auto& p : vars) {
        p.second.currentValue = p.second.defaultValue;
    }
}

VkDeviceSize Renderer::getGlobalUniformsSize() 
{
    VkPhysicalDeviceProperties properties = Device::getPhysicalDevice().GetProperties();
    VkDeviceSize alignment = properties.limits.minUniformBufferOffsetAlignment;
    VkDeviceSize globalDataSize = sizeof(GlobalUniforms);
    VkDeviceSize size = globalDataSize;
    if (globalDataSize % alignment > 0) {
        size = (globalDataSize / alignment + 1) * alignment;
    }
    return size;
}

void Renderer::updateUniforms()
{
    GlobalUniforms ubo;

    ubo.view = glm::mat4(1.0f);
    ubo.projection = glm::mat4(1.0f);
    ubo.viewPosition = glm::vec3(0.0f);

    if (activeCamera) {
        ubo.view = activeCamera->GetViewMatrix();
        ubo.projection = activeCamera->GetProjectionMatrix();
        ubo.viewPosition = activeCamera->GetPosition();
    }
    else {
        ubo.view = glm::lookAt(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        ubo.projection = glm::perspective(glm::radians(45.0f), swapchain.getExtent().width / (float)swapchain.getExtent().height, 0.1f, 10.0f);
        ubo.projection[1][1] *= -1;
    }
    
    VkDeviceSize offset = 0;

    void* globalData;
    Device::MapMemory(uniformBuffer.getMemory(), offset, sizeof(ubo), &globalData);
    memcpy(globalData, &ubo, sizeof(ubo));
    Device::UnmapMemory(uniformBuffer.getMemory());

    offset = getGlobalUniformsSize();

    for (auto& rc : renderComponents) {
        ObjectUniforms uniforms;
        rc->UpdateUniforms(&uniforms);

        void* objectData;
        Device::MapMemory(uniformBuffer.getMemory(), offset, sizeof(uniforms), &objectData);
        memcpy(objectData, &uniforms, sizeof(uniforms));
        Device::UnmapMemory(uniformBuffer.getMemory());
        offset += sizeof(ObjectUniforms);
    }
}

void Renderer::CreateBuffersAndCommands()
{
    createBuffers();

    globalDescriptorPool.create(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, 1);
    uint32_t objectsCount = static_cast<uint32_t>(renderComponents.Size());
    if (objectsCount > 0) {
        objectDescriptorPool.create(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, objectsCount, objectsCount);
    }
    createDescriptorSets();

    uint32_t meshCount = 0;
    for (auto& p : ResourceManager::models) {
        meshCount += p.second->getMeshCount();
    }

    if (meshCount > 0) {
        Mesh::textureDescriptorPool.create(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, meshCount, meshCount * 4);
    }

    for (auto& p : ResourceManager::models) {
        p.second->createDescriptorSets(meshSetLayout, 4, textureSampler);
    }

    recordCommands();
}

void Renderer::DestroyBuffersAndCommands() {
    Device::WaitIdle();

    auto commandsCount = static_cast<uint32_t>(commands.size());
    if (commandsCount > 0) {
        CommandPool::Free(commandsCount, commands.data());
    }

    vertexIndexBuffer.cleanup();
    uniformBuffer.cleanup();

    globalDescriptorPool.cleanup();
    objectDescriptorPool.cleanup();
    Mesh::textureDescriptorPool.cleanup();
}

void Renderer::Pause()
{
    renderComponents.Pause();
}

void Renderer::Resume()
{
    renderComponents.Resume();
}

void Renderer::ClearData()
{
    renderComponents.ClearAll();
}

void Renderer::createDescriptorSets()
{
    globalDescriptorPool.Allocate(1, &globalSetLayout, &globalDescriptorSet);
 
    VkDeviceSize offset = 0;
    auto globalDataSize = getGlobalUniformsSize();

    VkDescriptorBufferInfo globalBufferInfo{};
    globalBufferInfo.buffer = uniformBuffer.get();
    globalBufferInfo.offset = offset;
    globalBufferInfo.range = globalDataSize;

    VkWriteDescriptorSet globalDescriptorWrite{};
    globalDescriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    globalDescriptorWrite.dstSet = globalDescriptorSet;
    globalDescriptorWrite.dstBinding = 0;
    globalDescriptorWrite.dstArrayElement = 0;
    globalDescriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    globalDescriptorWrite.descriptorCount = 1;
    globalDescriptorWrite.pBufferInfo = &globalBufferInfo;

    Device::UpdateDescriptorSets(1, &globalDescriptorWrite, 0, nullptr);

    offset += getGlobalUniformsSize();

    size_t objectSize = renderComponents.Size();
    if (objectSize == 0) {
        return;
    }

    objectDescriptorSets.resize(objectSize);
    std::vector<VkDescriptorSetLayout> objectLayouts(objectSize, objectSetLayout);
    objectDescriptorPool.Allocate(objectSize, objectLayouts.data(), objectDescriptorSets.data());
    for (size_t i = 0; i < objectSize; ++i) {
        VkDescriptorBufferInfo bufferInfo{};
        bufferInfo.buffer = uniformBuffer.get();
        bufferInfo.offset = offset;
        bufferInfo.range = sizeof(ObjectUniforms);

        VkWriteDescriptorSet descriptorWrite{};

        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = objectDescriptorSets[i];
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.pBufferInfo = &bufferInfo;

        Device::UpdateDescriptorSets(1, &descriptorWrite, 0, nullptr);

        renderComponents[i]->descriptorSetIndex = i;

        offset += sizeof(ObjectUniforms);
    }
}

void Renderer::recordCommands()
{
    commands.resize(swapchain.getImages().size());
    CommandPool::Allocate(static_cast<uint32_t>(commands.size()), commands.data());
    
    std::cout << "Recording commands... ";

    for (size_t i = 0; i < commands.size(); ++i) {
        std::cout << i << "... ";
        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = 0;
        beginInfo.pInheritanceInfo = nullptr;

        if (vkBeginCommandBuffer(commands[i], &beginInfo) != VK_SUCCESS) {
            throw std::runtime_error("failed to begin recording to command buffer");
        }

        swapchain.currentRecordingTarget = i;

        graph.Execute(commands[i]);

        swapchain.Display(commands[i], images.at("swapchain"), i);
        
        if (vkEndCommandBuffer(commands[i]) != VK_SUCCESS) {
            throw std::runtime_error("failed to record command buffer!");
        }
    }

    std::cout << "done" << std::endl;
}

void Renderer::ExecuteRenderPass(VkCommandBuffer command, uint32_t id, std::vector<ImageData>& targetsData, std::vector<GroupData>& groups)
{
    /*if (renderPasses.find(id) == renderPasses.end()) {
        CreateRenderPass(id, imageData);
    }*/

    // imageData -> target
    Framebuffer framebuffer = GetFramebuffer(id, targetsData);

    // groups -> components
    auto components = GetComponents(groups);

    // TODO: get from graph
    glm::vec3 clearColor(0.0f);

    renderPasses.at(id).recordCommands(command, framebuffer, components, clearColor);
}

void Renderer::ExecutePostProcess(VkCommandBuffer command, uint32_t id, std::vector<ImageData>& targetsData, std::vector<VkImageView>& inputViews)
{
    std::string quadName = "quad" + std::to_string(id);

    Framebuffer framebuffer = GetFramebuffer(id, targetsData);

    RenderPassData data;
    if (!GetVar<RenderPassData>(std::to_string(id), &data)) {
        std::cerr << "No render pass data found for id: " << id << std::endl;
        return;
    }

    renderPasses.at(id).recordPostprocessCommands(command, framebuffer, quadName, data.shaderName);
}

Framebuffer Renderer::GetFramebuffer(uint32_t rpId, std::vector<ImageData>& imagesData)
{
    /*if (imageData.isSwapchain) {
        return swapchain.getFramebuffers(rpId)[swapchain.currentRecordingTarget];
    }*/

    RenderTarget& target = targets.at(rpId);

    target.createFramebuffer(rpId, renderPasses.at(rpId));
    return target.getFramebuffer(rpId);
}

std::vector<std::shared_ptr<RenderComponent>> Renderer::GetComponents(std::vector<GroupData>& groups)
{
    std::vector<std::shared_ptr<RenderComponent>> ret;
    for (auto& rc : renderComponents) {
        if (std::any_of(groups.begin(), groups.end(), [rc](GroupData data) {
            return data.name == rc->groupName; })) {
            ret.push_back(rc);
        }
    }
    return ret;
}

void Renderer::CreateRenderTarget(uint32_t rpId, std::vector<ImageData>& imagesData)
{
    if (targets.find(rpId) == targets.end()) {
        targets[rpId].create(imagesData, true, swapchain.getExtent());
        //targets[name].create(swapchain.getExtent(), swapchain.getImageFormat());
    }
}

void Renderer::DestroyRenderTarget(uint32_t rpId)
{
    if (targets.find(rpId) != targets.end()) {
        targets[rpId].cleanup();
        targets.erase(rpId);
    }
}

void Renderer::createTextureSampler()
{
    VkSamplerCreateInfo samplerInfo{};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16.0f;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;

    Device::CreateSampler(&samplerInfo, &textureSampler);
}

void Renderer::createDescriptorSetLayouts()
{
    // Global uniforms ---- view, projection

    VkDescriptorSetLayoutBinding globalUniformLayoutBinding{};
    globalUniformLayoutBinding.binding = 0;
    globalUniformLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    globalUniformLayoutBinding.descriptorCount = 1;
    globalUniformLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    globalUniformLayoutBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo globalLayoutInfo{};
    globalLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    globalLayoutInfo.bindingCount = 1;
    globalLayoutInfo.pBindings = &globalUniformLayoutBinding;

    Device::CreateDescriptorSetLayout(&globalLayoutInfo, &globalSetLayout);

    // Per object uniforms ---- model matrix

    VkDescriptorSetLayoutBinding objectUniformLayoutBinding{};
    objectUniformLayoutBinding.binding = 0;
    objectUniformLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    objectUniformLayoutBinding.descriptorCount = 1;
    objectUniformLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    objectUniformLayoutBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo objectLayoutInfo{};
    objectLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    objectLayoutInfo.bindingCount = 1;
    objectLayoutInfo.pBindings = &objectUniformLayoutBinding;

    Device::CreateDescriptorSetLayout(&objectLayoutInfo, &objectSetLayout);

    // Per mesh uniforms ---- samplers

    VkDescriptorSetLayoutBinding samplersBinding{};
    samplersBinding.binding = 0;
    samplersBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplersBinding.descriptorCount = 4;
    samplersBinding.pImmutableSamplers = nullptr;
    samplersBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorSetLayoutCreateInfo meshLayoutInfo{};
    meshLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    meshLayoutInfo.bindingCount = 1;
    meshLayoutInfo.pBindings = &samplersBinding;

    Device::CreateDescriptorSetLayout(&meshLayoutInfo, &meshSetLayout);
}