#pragma once

#include <string>
#include <glm/glm.hpp>
#include <optional>
#include "vk.h"

struct GlobalUniforms {
	alignas(16) glm::mat4 view;
	alignas(16) glm::mat4 projection;
	alignas(4) glm::vec3 viewPosition;
};

struct ObjectUniforms {
	alignas(16) glm::mat4 model;
};

template<typename T>
struct VarData {
	T currentValue;
	T defaultValue;
};

struct ImageData {
	std::string name;
	std::optional<VkFormat> format;
	std::optional<VkExtent2D> extent;
	bool isSwapchain;

	bool operator==(const ImageData& other) const {
		return name == other.name;//&& format == other.format && extent == other.extent && isSwapchain == other.isSwapchain;
	}

	bool operator!=(const ImageData& other) const {
		return (*this == other) == false;
	}
};

struct GroupData {
	std::string name;

	bool operator==(const GroupData& other) const {
		return name == other.name;
	}

	bool operator!=(const GroupData& other) const {
		return (*this == other) == false;
	}
};

struct RenderPassData {
	bool clearColor;
	bool clearDepth;
	std::string shaderName;
	bool depthTest;

	bool operator==(const RenderPassData& other) const {
		return clearColor == other.clearColor && clearDepth == other.clearDepth && shaderName == other.shaderName && depthTest == other.depthTest;
	}

	bool operator!=(const RenderPassData& other) const {
		return (*this == other) == false;
	}
};