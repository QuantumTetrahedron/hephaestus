#pragma once
#include "vk.h"
#include "../Core/RenderComponent.h"
#include <optional>

class Framebuffer;
class CameraComponent;

class RenderPass
{
public:
	struct Config {
		VkFormat format;
		VkAttachmentLoadOp loadOp;
		VkAttachmentStoreOp storeOp;
		VkImageLayout initialLayout;
		VkImageLayout finalLayout;
	};

	void create(uint32_t id, std::vector<Config>& configs, std::optional<Config> depthConfig, std::shared_ptr<Shader> shader);
	void cleanup();

	Config createConfig(VkFormat imageFormat, bool clear, bool isDepth, bool isSwapchain);

	void recordCommands(VkCommandBuffer buffer, Framebuffer& target, std::vector<std::shared_ptr<RenderComponent>>& components, glm::vec3 clearColor);
	void recordPostprocessCommands(VkCommandBuffer buffer, Framebuffer& target, std::string quadName, std::string shaderName);

	VkRenderPass get() const;
	int colorAttachmentsCount;
	bool hasDepthAttachment;
private:
	VkRenderPass renderPass;
	std::weak_ptr<Shader> _shader;
	uint32_t _id;
};

