#include "CommandPool.h"
#include "Device.h"

VkCommandPool CommandPool::commandPool;

void CommandPool::create()
{
	QueueFamilyIndices indices = Device::getPhysicalDevice().getQueueFamilies();

	VkCommandPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = indices.graphicsFamily.value();
	poolInfo.flags = 0;

	Device::CreateCommandPool(&poolInfo, &commandPool);
}

void CommandPool::cleanup()
{
	Device::DestroyCommandPool(commandPool);
}

void CommandPool::Allocate(uint32_t count, VkCommandBuffer* commands)
{
	VkCommandBufferAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = count;
	Device::AllocateCommandBuffers(&allocInfo, commands);
}

void CommandPool::Free(uint32_t count, VkCommandBuffer* commands)
{
	Device::FreeCommandBuffers(commandPool, count, commands);
}

VkCommandBuffer CommandPool::beginSingleTimeCommands()
{
	VkCommandBuffer command;
	Allocate(1, &command);

	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(command, &beginInfo);

	return command;
}

void CommandPool::endSingleTimeCommands(VkCommandBuffer command)
{

	vkEndCommandBuffer(command);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &command;

	vkQueueSubmit(Device::getGraphicsQueue(), 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(Device::getGraphicsQueue());

	Free(1, &command);
}
