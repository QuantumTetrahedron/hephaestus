#pragma once
#include "vk.h"

class DescriptorPool
{
public:
	void create(VkDescriptorType type, uint32_t setsCount, uint32_t descriptorCount);
	void cleanup();

	void Allocate(uint32_t count,const VkDescriptorSetLayout* layouts, VkDescriptorSet* sets);
	void Free(uint32_t descriptorCount, const VkDescriptorSet* sets);
private:
	VkDescriptorPool pool;
};

