#pragma once
#include "vk.h"
#include <map>
#include <string>
#include <vector>

class VGNode;
class StartNode;

class VortexGraph {
public:
	bool LoadFromFile(const char* path);
	void Execute(VkCommandBuffer command);
	void Cleanup();
	void Initialize();
	void Reset();
private:
	void RemoveLeadingSpaces(const std::string& line, size_t& begin, size_t& end) const;
	void RemoveTrailingSpaces(const std::string& line, size_t& begin, size_t& end) const;
	void RemoveComment(const std::string& line, size_t& begin, size_t& end) const;
	
	void ParseContent(const std::string& line);
	void ParseInt(const std::string& name, const std::string& data);
	void ParseFloat(const std::string& name, const std::string& data);
	void ParseBool(const std::string& name, const std::string& data);
	void ParseImage(const std::string& name, const std::string& data);
	void ParseGroup(const std::string& name, const std::string& data);
	void ParseRenderpass(const std::string& name, const std::string& data);
	void ParseGraph(const std::string& name, const std::string& data);
	void ParseGraphNode(const std::string& data);
	void ParseGraphEdge(const std::string& from, const std::string& to);
	std::map<std::string, std::string> ParseVarData(const std::string& data);

	std::vector<VGNode*> nodes;
	StartNode* startNode;
};