#include "VortexGraphNodes.h"
#include "../Resources/ResourceManager.h"
#include "Device.h"

VGNode* VGNodeFactory::LoadNode(const std::string& code)
{
	// split dots
	std::stringstream ss(code);
	std::vector<std::string> split;
	std::string data;
	while (std::getline(ss, data, '.')) {
		split.push_back(data);
	}

	int id = std::stoi(split[split.size() - 1]);

	const std::string& n = split[0];
	if (n == "S" && split.size() == 2) {
		return new StartNode(id);
	}
	if (n == "I" && split.size() == 2) {
		return new IfNode(id);
	}
	if (n == "W" && split.size() == 2) {
		return new WhileNode(id);
	}
	if (n == "SC" && split.size() == 2) {
		return new SwapchainNode(id);
	}
	if (n == "RP" && split.size() == 2) {
		return new RenderPassNode(id);
	}
	if (n == "O") {
		return LoadOperatorNode(split, id);
	}
	if (n == "GET") {
		return LoadGetterNode(split, id);
	}
	if (n == "SET") {
		return LoadSetterNode(split, id);
	}
	if (n == "SEL") {
		return LoadSelectNode(split, id);
	}

	return nullptr;
}

VGNode* VGNodeFactory::LoadOperatorNode(const std::vector<std::string>& split, int id)
{
	const std::string& operatorCode = split[1];
	
	if (split.size() == 3) {
		if (operatorCode == "and") {
			return new OperatorNode2<bool, bool, bool>([](bool a, bool b) { return a && b; }, id);
		}
		if (operatorCode == "or") {
			return new OperatorNode2<bool, bool, bool>([](bool a, bool b) {return a || b; }, id);
		}
		if (operatorCode == "not") {
			return new OperatorNode1<bool, bool>([](bool a) {return !a; }, id);
		}
	}

	if (split.size() == 4) {
		const std::string& variant = split[2];
		if (variant == "0") {
			return LoadOperatorNodeT<int, int, int>(operatorCode, id);
		}
		if (variant == "1") {
			return LoadOperatorNodeT<int, float, float>(operatorCode, id);
		}
		if (variant == "2") {
			return LoadOperatorNodeT<float, int, float>(operatorCode, id);
		}
		if (variant == "3") {
			return LoadOperatorNodeT<float, float, float>(operatorCode, id);
		}
	}

	return nullptr;
}

VGNode* VGNodeFactory::LoadGetterNode(const std::vector<std::string>& split, int id)
{
	const std::string& varName = split[1];

	int isInt;
	float isFloat;
	bool isBool;
	ImageData isImage;
	GroupData isGroup;

	if (Renderer::GetVar(varName, &isInt)) {
		return new VarGetterNode<int>(varName, id);
	}

	if (Renderer::GetVar(varName, &isFloat)) {
		return new VarGetterNode<float>(varName, id);
	}

	if (Renderer::GetVar(varName, &isBool)) {
		return new VarGetterNode<bool>(varName, id);
	}

	if (Renderer::GetVar(varName, &isImage)) {
		return new VarGetterNode<ImageData>(varName, id);
	}

	if (Renderer::GetVar(varName, &isGroup)) {
		return new VarGetterNode<GroupData>(varName, id);
	}

	return nullptr;
}

VGNode* VGNodeFactory::LoadSelectNode(const std::vector<std::string>& split, int id)
{
	const std::string& type = split[1];

	if (type == "int") {
		return new VarSelectNode<int>(id);
	}
	else if (type == "float") {
		return new VarSelectNode<float>(id);
	}
	else if (type == "bool") {
		return new VarSelectNode<bool>(id);
	}
	else if (type == "image") {
		return new VarSelectNode<ImageData>(id);
	}
	else if (type == "group") {
		return new VarSelectNode<GroupData>(id);
	}

	std::cerr << "Unknown type for select node: " << type << std::endl;
	return nullptr;
}

VGNode* VGNodeFactory::LoadSetterNode(const std::vector<std::string>& split, int id)
{
	const std::string& varName = split[1];

	int isInt;
	float isFloat;
	bool isBool;
	ImageData isImage;
	GroupData isGroup;

	if (Renderer::GetVar(varName, &isInt)) {
		return new VarSetterNode<int>(varName, id);
	}

	if (Renderer::GetVar(varName, &isFloat)) {
		return new VarSetterNode<float>(varName, id);
	}

	if (Renderer::GetVar(varName, &isBool)) {
		return new VarSetterNode<bool>(varName, id);
	}

	if (Renderer::GetVar(varName, &isImage)) {
		return new VarSetterNode<ImageData>(varName, id);
	}

	if (Renderer::GetVar(varName, &isGroup)) {
		return new VarSetterNode<GroupData>(varName, id);
	}

	return nullptr;
}

VGNode::VGNode(int id)
: _id(id) {}

bool VGNode::is(int id)
{
	return _id == id;
}

void VGNode::Connect(VGNode* other, const std::string& port, const std::string& otherPort)
{
	if (other == nullptr) {
		return;
	}

	ConnectToPort(port, other);
	other->ConnectToPort(otherPort, this);
}

void VGNode::AddPort(const std::string& name)
{
	_ports[name] = std::vector<VGNode*>();
}

void VGNode::ConnectToPort(const std::string& name, VGNode* node)
{
	try {
		auto& nodes = _ports.at(name);
		nodes.push_back(node);
	}
	catch (...) {}
}

const std::vector<VGNode*>& VGNode::GetPort(const std::string& name) const
{
	return _ports.at(name);
}

StartNode::StartNode(int id) : VGNode(id)
{
	AddPort("next");
}

void StartNode::Run(nodeFunc func, RunConfig& config)
{
	if (!Visit(func, config)) return;
	auto next = GetNode<VGExecutable>("next");
	if (next) next->Run(func, config);
}

RenderPassNode::RenderPassNode(int id) : VGNode(id)
{
	AddPort("prev");
	AddPort("next");
	AddPort("groups");
	AddPort("inputImage1");
	AddPort("inputImage2");
	AddPort("inputImage3");
	AddPort("inputImage4");
	AddPort("target1");
	AddPort("target2");
	AddPort("target3");
	AddPort("target4");
}

void RenderPassNode::Run(nodeFunc func, RunConfig& config)
{
	if (!Visit(func, config)) return;

	auto next = GetNode<VGExecutable>("next");
	if (next) next->Run(func, config);
}

void RenderPassNode::Execute(VkCommandBuffer buffer)
{
	VGGetter<ImageData>* targetDataNode1 = GetNode<VGGetter<ImageData>>("target1");
	VGGetter<ImageData>* targetDataNode2 = GetNode<VGGetter<ImageData>>("target2");
	VGGetter<ImageData>* targetDataNode3 = GetNode<VGGetter<ImageData>>("target3");
	VGGetter<ImageData>* targetDataNode4 = GetNode<VGGetter<ImageData>>("target4");

	std::vector<ImageData> targetsData;

	if (targetDataNode1 != nullptr) {
		targetsData.push_back(targetDataNode1->Get());
	}
	if (targetDataNode2 != nullptr) {
		targetsData.push_back(targetDataNode2->Get());
	}
	if (targetDataNode3 != nullptr) {
		targetsData.push_back(targetDataNode3->Get());
	}
	if (targetDataNode4 != nullptr) {
		targetsData.push_back(targetDataNode4->Get());
	}

	if (!inputGroups.empty()) {
		Renderer::ExecuteRenderPass(buffer, _id, targetsData, inputGroups);
	}
	else {
		Renderer::ExecutePostProcess(buffer, _id, targetsData, inputImageViews);
	}
}

void RenderPassNode::Init()
{
	std::vector<std::string> outputs = { "target1", "target2", "target3", "target4" };
	std::vector<ImageData> targetsData;
	for (int i = 0; i < outputs.size(); ++i) {
		VGGetter<ImageData>* targetDataNode = GetNode<VGGetter<ImageData>>(outputs[i]);
		if (targetDataNode) {
			ImageData targetData = targetDataNode->Get();
			targetsData.push_back(targetData);
		}
	}

	std::vector<VGGetter<GroupData>*> groupNodes = GetNodes<VGGetter<GroupData>>("groups");

	if (!groupNodes.empty()) {
		std::transform(groupNodes.begin(), groupNodes.end(), std::back_inserter(inputGroups), [](VGGetter<GroupData>* getter) {return getter->Get(); });
		Renderer::CreateRenderPass(_id, targetsData);
	}
	else {
		Renderer::CreateRenderPass(_id, targetsData);
		std::vector<std::string> inputs = { "inputImage1", "inputImage2", "inputImage3", "inputImage4" };
		for (int i = 0; i < inputs.size(); ++i) {
			VGGetter<ImageData>* node = GetNode<VGGetter<ImageData>>(inputs[i]);
			if (node) {
				ImageData image = node->Get();
				inputImages.push_back(image);
			}
		}

		for (int i = 0; i < inputImages.size(); ++i) {
			VkImageView view = Renderer::getImage(inputImages[i].name)->createImageView();
			inputImageViews.push_back(view);
		}

		ResourceManager::LoadQuadModel("quad" + std::to_string(_id), inputImageViews);
	}
}

void RenderPassNode::Clean()
{
	Renderer::CleanupRenderPass(_id);

	std::string quadName = "quad" + std::to_string(_id);
	if (ResourceManager::HasModel(quadName)) {
		ResourceManager::DeleteModel(quadName);
	}

	for (int i = 0; i < inputImageViews.size(); ++i) {
		Device::DestroyImageView(inputImageViews[i]);
	}

	inputImageViews.clear();
	inputImages.clear();
}

void RenderPassNode::Reset()
{
}

IfNode::IfNode(int id) : VGNode(id)
{
	AddPort("prev");
	AddPort("true");
	AddPort("false");
	AddPort("condition");
}

void IfNode::Run(nodeFunc func, RunConfig& config)
{
	if (!Visit(func, config)) return;

	bool condition = false;

	if (!config.broadcast) {
		VGGetter<bool>* condNode = GetNode<VGGetter<bool>>("condition");
		if (!condNode) {
			std::cerr << "No condition provided to If node: " << _id << std::endl;
			return;
		}
		condition = condNode->Get();
	}

	if (condition || config.broadcast) {
		VGExecutable* trueNode = GetNode<VGExecutable>("true");
		if (trueNode) trueNode->Run(func, config);
	}
	if (!condition || config.broadcast) {
		VGExecutable* falseNode = GetNode<VGExecutable>("false");
		if (falseNode) falseNode->Run(func, config);
	}
}

WhileNode::WhileNode(int id) : VGNode(id)
{
	AddPort("prev");
	AddPort("do");
	AddPort("then");
	AddPort("condition");
}

void WhileNode::Run(nodeFunc func, RunConfig& config)
{
	if (!Visit(func, config)) return;

	VGExecutable* doNode = GetNode<VGExecutable>("do");

	if (config.broadcast) {
		if (doNode) doNode->Run(func, config);
	}
	else {
		auto condNode = GetNode<VGGetter<bool>>("condition");
		if (!condNode) {
			std::cerr << "No condition provided to While node: " << _id << std::endl;
		}
		else {
			while (condNode->Get()) {
				if (doNode) doNode->Run(func, config);
			}
		}
	}

	VGExecutable* thenNode = GetNode<VGExecutable>("then");
	if (thenNode) thenNode->Run(func, config);
}

ImageNode::ImageNode(const std::string& name, int id) : VarGetterNode<ImageData>(name, id)
{
	AddPort("write");
	AddPort("read");
}

SwapchainNode::SwapchainNode(int id) : VGNode(id) {
	AddPort("write");
}

ImageData SwapchainNode::Get() {
	ImageData data;
	data.isSwapchain = true;
	data.name = "swapchain";
	return data;
}

void VGExecutable::Initialize()
{
	if (!active) {
		Init();
		active = true;
	}
}

void VGExecutable::Cleanup()
{
	if (active) {
		Clean();
		active = false;
	}
}

bool VGExecutable::Visit(nodeFunc func, RunConfig& config)
{
	if (config.broadcast && config.visited.find(this) != config.visited.end()) {
		return false;
	}

	func(this, config.command);
	config.visited.insert(this);
	return true;
}
