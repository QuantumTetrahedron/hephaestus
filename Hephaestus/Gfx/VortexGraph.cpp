#include "VortexGraph.h"

#include "VortexGraphNodes.h"
#include "Renderer.h"
#include "../Resources/ResourceManager.h"
#include <iostream>
#include <sstream>
#include <fstream>

bool VortexGraph::LoadFromFile(const char* path)
{
	std::ifstream file(path, std::ios::in);
    if (!file.is_open()) {
        return false;
    }

    std::vector<std::string> contents;
    std::string line;

    std::string currentContent;
    while (std::getline(file, line)) {
        size_t begin = 0;
        size_t end = line.length();

        RemoveComment(line, begin, end);
        RemoveLeadingSpaces(line, begin, end);

        // ignore empty
        if (begin >= end) continue;

        // add to current content
        currentContent += line.substr(begin, end - begin);

        size_t i = 0;
        int bracketCount = 0;
        for (; i < currentContent.length(); ++i) {
            if (currentContent[i] == '{') {
                bracketCount++;
                if (bracketCount > 1 || bracketCount < 0) {
                    std::cerr << "Error reading content: " << currentContent << std::endl;
                }
            }

            if (currentContent[i] == '}') {
                bracketCount--;
                if (bracketCount > 1 || bracketCount < 0) {
                    std::cerr << "Error reading content: " << currentContent << std::endl;
                }
            }

            if (currentContent[i] == ';' && bracketCount == 0) {
                std::string c = currentContent.substr(0, i+1);
                contents.push_back(c);
                currentContent = currentContent.substr(i+1);
                i = 0;
                bracketCount = 0;
            }
        }
    }

    for (const std::string& content : contents) {
        ParseContent(content);
    }

    file.close();
    return true;
}

void VortexGraph::Execute(VkCommandBuffer command)
{
    if (!startNode) return;

    VGExecutable::RunConfig config{};
    config.broadcast = false;
    config.command = command;

    startNode->Run([](VGExecutable* node, VkCommandBuffer command) {
        node->Execute(command);
    }, config);
}

void VortexGraph::Cleanup()
{
    if (startNode) {
        VGExecutable::RunConfig config{};
        config.broadcast = true;
        startNode->Run([](VGExecutable* node, VkCommandBuffer command) {
            node->Cleanup();
        }, config);
    }

    startNode = nullptr;
    for (int i = 0; i < nodes.size(); i++) {
        delete nodes[i];
        nodes[i] = nullptr;
    }
    nodes.clear();
    Renderer::CleanupGraph();
}

void VortexGraph::Initialize()
{
    if (startNode != nullptr) {
        VGExecutable::RunConfig config{};
        config.broadcast = true;
        startNode->Run([](VGExecutable* node, VkCommandBuffer command) {
            node->Initialize();
        }, config);
    }
}

void VortexGraph::Reset()
{
    if (startNode) {
        VGExecutable::RunConfig config{};
        config.broadcast = true;
        startNode->Run([](VGExecutable* node, VkCommandBuffer command) {
            node->Cleanup();
            }, config);
    }

    Initialize();
}

void VortexGraph::RemoveLeadingSpaces(const std::string& line, size_t& begin, size_t& end) const {
    if (begin >= end) return;
    while (line[begin] == ' ' || line[begin] == '\t') {
        if (begin == std::string::npos)break;
        ++begin;
    }
}

void VortexGraph::RemoveTrailingSpaces(const std::string& line, size_t& begin, size_t& end) const {
    if (begin >= end) return;
    while (line[end] == ' ' || line[end] == '\t') {
        --end;
    }
}

void VortexGraph::RemoveComment(const std::string& line, size_t& begin, size_t& end) const
{
    if (begin >= end) return;
    end = line.find_first_of("#");
}

void VortexGraph::ParseContent(const std::string& line)
{
    size_t begin = 0;
    size_t end = line.length() - 1;
    RemoveLeadingSpaces(line, begin, end);

    std::string type = "";
    std::string rest = "";
    for (size_t i = begin; i <= end; ++i) {
        if (line[i] == ' ' || line[i] == '\t') {
            type = line.substr(begin, i - begin);
            size_t restBegin = i;
            RemoveLeadingSpaces(line, restBegin, end);
            rest = line.substr(restBegin, end - restBegin);
            break;
        }
    }

    std::string name;
    std::string data;
    begin = 0;
    end = rest.length() - 1;

    size_t separator = rest.find_first_of('=');
    size_t openBracket = rest.find_first_of('{');
    separator = openBracket < separator ? openBracket : separator;

    size_t lEnd = separator - 1;
    size_t rBegin = separator + 1;

    if (lEnd <= end) {
        RemoveTrailingSpaces(rest, begin, lEnd);
        name = rest.substr(begin, lEnd - begin + 1);
        RemoveLeadingSpaces(rest, rBegin, end);
        RemoveTrailingSpaces(rest, rBegin, end);
        data = rest.substr(rBegin, end - rBegin + 1);

        end = data.length() - 1;
        if (end >= 0 && data[end] == '}') {
            data = data.substr(0, end);
        }
    }
    else {
        name = rest;
        data = "";
    }

    if (type == "int") ParseInt(name, data);
    else if (type == "float") ParseFloat(name, data);
    else if (type == "bool") ParseBool(name, data);
    else if (type == "image") ParseImage(name, data);
    else if (type == "group") ParseGroup(name, data);
    else if (type == "renderpass") ParseRenderpass(name, data);
    else if (type == "graph") ParseGraph(name, data);
    else {
        std::cerr << "Error reading line: " << line << std::endl;
    }
}

void VortexGraph::ParseInt(const std::string& name, const std::string& data)
{
    int value = 0;
    if(data != "") {
        value = std::stoi(data);
    }
    Renderer::AddVar(name, value);
}

void VortexGraph::ParseFloat(const std::string& name, const std::string& data)
{
    float value = 0.f;
    if(data != "") {
        value = std::stof(data);
    }
    Renderer::AddVar(name, value);
}

void VortexGraph::ParseBool(const std::string& name, const std::string& data)
{
    bool value = false;
    if(data != "") {
        if (data == "true") {
            value = true;
        }
        else if (data == "false") {
            value = false;
        }
        else {
            try {
                float f = std::stof(data);
                value = f != 0;
            }
            catch (...) {
                std::cerr << "Error parsing bool: " << data << std::endl;
                return;
            }
        }
    }
    Renderer::AddVar(name, value);
}

void VortexGraph::ParseImage(const std::string& name, const std::string& data)
{
    auto content = ParseVarData(data);

    ImageData imageData;
    imageData.name = name;
    imageData.isSwapchain = false;

    if (content.find("format") != content.end()) {
        imageData.format = (VkFormat)stoi(content.at("format"));
    }

    if (content.find("width") != content.end() && content.find("height") != content.end()) {
        int w = stoi(content.at("width"));
        int h = stoi(content.at("height"));
        VkExtent2D ext{ w, h };
        imageData.extent = ext;
    }

    Renderer::CreateImage(imageData);
    Renderer::AddVar(name, imageData);
}

void VortexGraph::ParseGroup(const std::string& name, const std::string& data)
{
    GroupData groupData;
    groupData.name = name;
    Renderer::AddVar(name, groupData);
}

void VortexGraph::ParseRenderpass(const std::string& name, const std::string& data)
{
    auto content = ParseVarData(data);

    std::string& clearColorData = content["clearColor"];
    std::string& clearDepthData = content["clearDepth"];
    
    RenderPassData d;
    // TODO: also load clear color
    d.clearColor = clearColorData == "true";
    d.clearDepth = clearDepthData == "true";

    // TODO: Load from graph
    d.depthTest = true;
    
    if (content.find("shader") != content.end()) {
        d.shaderName = content.at("shader");
    }

    Renderer::AddVar(name, d);
}

void VortexGraph::ParseGraph(const std::string& name, const std::string& data)
{
    std::string code;
    std::stringstream ss(data);
    std::string edgeDelimiter = "->";

    while (std::getline(ss, code, ';')) {
        size_t pos = code.find(edgeDelimiter);
        if (pos != std::string::npos) {
            std::string edgeFrom = code.substr(0, pos);
            std::string edgeTo = code.substr(pos + edgeDelimiter.length());
            ParseGraphEdge(edgeFrom, edgeTo);
        }
        else {
            ParseGraphNode(code);
        }
    }
}

void VortexGraph::ParseGraphNode(const std::string& str)
{
    size_t metaStart = str.find('[');
    size_t metaEnd = str.find(']');

    std::string data = str.substr(0, metaStart) + str.substr(metaEnd + 1);

    auto node = VGNodeFactory::LoadNode(data);
    if (node == nullptr) return;

    nodes.push_back(node);

    StartNode* start = dynamic_cast<StartNode*>(node);
    if (start != nullptr) {
        startNode = start;
    }
}

void VortexGraph::ParseGraphEdge(const std::string& from, const std::string& to)
{
    std::cout << from << " -> " << to << std::endl;

    std::stringstream fromStream(from);
    std::stringstream toStream(to);
    std::string fromId;
    std::string fromPort;
    std::string toId;
    std::string toPort;
    std::getline(fromStream, fromId, ':');
    std::getline(fromStream, fromPort, ':');
    std::getline(toStream, toId, ':');
    std::getline(toStream, toPort, ':');

    VGNode *fromNode = nullptr, *toNode = nullptr;
    for (VGNode* node : nodes) {
        if (node->is(stoi(fromId)))
            fromNode = node;
    }
    for (VGNode* node : nodes) {
        if (node->is(stoi(toId)))
            toNode = node;
    }

    if (fromNode && toNode) {
        fromNode->Connect(toNode, fromPort, toPort);
    }
}

std::map<std::string, std::string> VortexGraph::ParseVarData(const std::string& data)
{
    std::string line;
    std::stringstream ss(data);

    std::map<std::string, std::string> parsedContent;
    while (std::getline(ss, line, ';')) {
        size_t begin = 0;
        size_t end = line.length() - 1;
        RemoveLeadingSpaces(line, begin, end);

        std::string name;
        std::string data;

        size_t separator = line.find_first_of('=');

        size_t lEnd = separator - 1;
        size_t rBegin = separator + 1;

        if (lEnd <= end) {
            RemoveTrailingSpaces(line, begin, lEnd);
            name = line.substr(begin, lEnd - begin + 1);
            RemoveLeadingSpaces(line, rBegin, end);
            RemoveTrailingSpaces(line, rBegin, end);
            data = line.substr(rBegin, end - rBegin + 1);
            parsedContent[name] = data;
        }
    }
    return parsedContent;
}
