#pragma once
#include <optional>
#include <vector>
#include "vk.h"
	
struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;

	bool isComplete() {
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

class Instance;
class Surface;

class PhysicalDevice
{
public:
	void pick(const Instance& instance, const Surface& surface);
	VkPhysicalDevice get() const;

	QueueFamilyIndices getQueueFamilies() const;
	SwapChainSupportDetails getSwapChainSupport(const Surface& surface) const;

	const std::vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	VkPhysicalDeviceProperties GetProperties();
	void GetMemoryProperties(VkPhysicalDeviceMemoryProperties* properties);
private:
	bool isDeviceSuitable(VkPhysicalDevice device, const Surface& surface) const;
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, const Surface& surface) const;

	bool checkDeviceExtensionSupport(VkPhysicalDevice device) const;
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, const Surface& surface) const;

	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	QueueFamilyIndices queueFamilies;
	SwapChainSupportDetails swapChainSupportDetails;
	std::optional<VkPhysicalDeviceProperties> properties;
};

