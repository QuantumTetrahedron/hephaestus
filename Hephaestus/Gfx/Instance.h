#pragma once
#include "Debug.h"

class Instance
{
public:
	void create(const std::vector<const char*>& extensions, bool _debug);
	void cleanup();

	VkInstance get() const;
	Debug debugManager;
private:
	VkInstance instance;
	bool debug;
};

