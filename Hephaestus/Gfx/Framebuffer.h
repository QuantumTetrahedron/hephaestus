#pragma once
#include "vk.h"
#include "RenderPass.h"

class Framebuffer {
public:
	void create(const std::vector<VkImageView>& colorImageViews, std::optional<VkImageView> depthImageView, const RenderPass& renderPass, VkExtent2D ext);
	void destroy();

	VkFramebuffer framebuffer;
	VkExtent2D extent;
	int colorAttachmentsCount;
	bool hasDepth;
};