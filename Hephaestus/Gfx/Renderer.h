#pragma once
#include <map>
#include "DescriptorPool.h"
#include "Buffer.h"
#include "Swapchain.h"
#include "../Core/RenderComponent.h"
#include "../Utils/PausableVector.h"
#include "../Core/CameraComponent.h"
#include "VortexGraph.h"
#include "RenderTarget.h"
#include <variant>
#include <optional>
#include "RendererData.h"

class Object;
class Model;
class Surface;
class RenderPassNode;

class Renderer
{
public:
	Renderer() = delete;

	static void LoadGraph(const std::string& vortexPath);
	static void CleanupGraph();

	static void Initialize(const Surface& surface, const std::string& vortexPath);
	static void Render(const Surface& surface);
	static void Cleanup();

	static void ActivateCamera(CameraComponent* camera);

	static void RegisterComponent(std::shared_ptr<RenderComponent> rc);
	static void UnregisterComponent(const std::shared_ptr<RenderComponent>& rc);

	static void OnResize();

	static std::vector<VkDescriptorSetLayout> GetDescriptorSetLayouts();

	using variant = std::variant<
		int, float, bool,
		ImageData, GroupData, RenderPassData
	>;

	static void CreateRenderPass(uint32_t id, std::vector<ImageData>& imageData);
	static void CleanupRenderPass(uint32_t id);
	static void ExecuteRenderPass(VkCommandBuffer command, uint32_t id, std::vector<ImageData>& targetsData, std::vector<GroupData>& groups);
	static void ExecutePostProcess(VkCommandBuffer command, uint32_t id, std::vector<ImageData>& targetsData, std::vector<VkImageView>& inputViews);

	static Framebuffer GetFramebuffer(uint32_t rpId, std::vector<ImageData>& imageData);
	static std::vector<std::shared_ptr<RenderComponent>> GetComponents(std::vector<GroupData>& groups);

	static void CreateRenderTarget(uint32_t rpId, std::vector<ImageData>& imagesData);
	static void DestroyRenderTarget(uint32_t rpId);
	static std::map<uint32_t, RenderTarget> targets;

	template<typename T>
	static bool GetVar(std::string name, T* out) {
		T* v = std::get_if<T>(&(vars[name].defaultValue));
		if (v != nullptr) {
			*out = *v;
			return true;
		}
		return false;
	}

	template<typename T>
	static void SetVar(std::string name, T value) {
		auto it = vars.find(name);
		if (it != vars.end()) {
			auto current = std::get_if<T>(&it->second.defaultValue);
			if (current && *current != value) {
				it->second.defaultValue = value;
				needsToReset = true;
			}
		}
		else {
			std::cerr << "No variable with name " << name << " found" << std::endl;
		}
	}

	template<typename T>
	static void AddVar(std::string name, T value) {
		auto it = vars.find(name);
		if (it == vars.end()) {
			vars[name].currentValue = vars[name].defaultValue = value;
			needsToReset = true;
		}
		else {
			SetVar<T>(name, value);
		}
	}

	template<typename T>
	static bool GetVarCurrent(std::string name, T* out) {
		T* v = std::get_if<T>(&(vars[name].currentValue));
		if (v != nullptr) {
			*out = *v;
			return true;
		}
		return false;
	}

	template<typename T>
	static void SetVarCurrent(std::string name, T value) {
		auto it = vars.find(name);
		if (it != vars.end()) {
			it->second.currentValue = value;
		}
	}

	static VkExtent2D getSwapchainExtent();

	static std::shared_ptr<Image> getImage(std::string name);
	static void CreateImage(ImageData& imageData);
	static std::shared_ptr<Image> getDepthImage();
private:
	static CameraComponent* activeCamera;

	static void Reset();
	static void ResetVars();

	static std::map<std::string, VarData<variant>> vars;
	static std::map<std::string, std::shared_ptr<Image>> images;
	static std::shared_ptr<Image> depthImage;

	static void updateUniforms();
	static VkDeviceSize getGlobalUniformsSize();

	static bool needsToLoadGraph;
	static std::string requestedVortexGraphPath;
	static VortexGraph graph;
	static std::map<uint32_t, RenderPass> renderPasses;
	static PausableVector<std::shared_ptr<RenderComponent>> renderComponents;

	static bool needsToRecreateBuffers;
	static bool needsToReset;

	static void CreateBuffersAndCommands();
	static void DestroyBuffersAndCommands();

	friend class SceneManager;
	friend class RenderPass;
	friend class Swapchain;

	static void Pause();
	static void Resume();
	static void ClearData();

	static bool framebufferResized;
	static void Resize(const Surface& surface);
	
	static DescriptorPool globalDescriptorPool;
	static DescriptorPool objectDescriptorPool;

	friend class ResourceManager;
	static Swapchain swapchain;

	static Buffer vertexIndexBuffer;
	static Buffer uniformBuffer;
	static VkSampler textureSampler;

	static void createBuffers();
	static void createDescriptorSets();
	static void recordCommands();
	static void createTextureSampler();
	
	static std::vector<VkCommandBuffer> commands;
	static VkDescriptorSet globalDescriptorSet;
	static std::vector<VkDescriptorSet> objectDescriptorSets;

	static void createDescriptorSetLayouts();
	static VkDescriptorSetLayout globalSetLayout;
	static VkDescriptorSetLayout objectSetLayout;
	static VkDescriptorSetLayout meshSetLayout;
};

