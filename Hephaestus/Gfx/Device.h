#pragma once
#include "PhysicalDevice.h"

class Instance;
class Surface;

class Device
{
public:
	static void create(const Instance& instance, const Surface& surface);
	static void cleanup();

	//static VkDevice get();
	static VkQueue getGraphicsQueue();
	static VkQueue getPresentQueue();

	static void SubmitToGraphicsQueue(uint32_t submitCount, const VkSubmitInfo* infos, VkFence fence);
	static VkResult Present(const VkPresentInfoKHR* presentInfo);

	static uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
	static VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
	static VkFormat findDepthFormat();

	static PhysicalDevice getPhysicalDevice();

	// Wrappers
	static void AllocateCommandBuffers(const VkCommandBufferAllocateInfo* allocInfo, VkCommandBuffer* commandBuffers);
	static void AllocateDescriptorSets(const VkDescriptorSetAllocateInfo* allocInfo, VkDescriptorSet* descriptorSets);
	static void AllocateMemory(const VkMemoryAllocateInfo* allocInfo, VkDeviceMemory* pDeviceMemory);
	static void BindBufferMemory(VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize memoryOffset);
	static void BindImageMemory(VkImage image, VkDeviceMemory memory, VkDeviceSize memoryOffset);
	static void CreateBuffer(const VkBufferCreateInfo* createInfo, VkBuffer* pBuffer);
	static void CreateCommandPool(const VkCommandPoolCreateInfo* createInfo, VkCommandPool* commandPool);
	static void CreateDescriptorPool(const VkDescriptorPoolCreateInfo* createInfo, VkDescriptorPool* pool);
	static void CreateDescriptorSetLayout(const VkDescriptorSetLayoutCreateInfo* createInfo, VkDescriptorSetLayout* layout);
	static void CreateFence(const VkFenceCreateInfo* createInfo, VkFence* fence);
	static void CreateFramebuffer(const VkFramebufferCreateInfo* createInfo, VkFramebuffer* framebuffer);
	static void CreateGraphicsPipelines(uint32_t createInfoCount, const VkGraphicsPipelineCreateInfo* createInfos, VkPipeline* pipelines);
	static void CreateImage(const VkImageCreateInfo* createInfo, VkImage* image);
	static void CreateImageView(const VkImageViewCreateInfo* createInfo, VkImageView* view);
	static void CreatePipelineLayout(const VkPipelineLayoutCreateInfo* createInfo, VkPipelineLayout* layout);
	static void CreateRenderPass(const VkRenderPassCreateInfo* createInfo, VkRenderPass* renderPass);
	static void CreateSampler(const VkSamplerCreateInfo* createInfo, VkSampler* sampler);
	static void CreateSemaphore(const VkSemaphoreCreateInfo* createInfo, VkSemaphore* semaphore);
	static void CreateShaderModule(const VkShaderModuleCreateInfo* createInfo, VkShaderModule* shaderModule);
	static void CreateSwapchain(const VkSwapchainCreateInfoKHR* createInfo, VkSwapchainKHR* swapchain);
	static void DestroyBuffer(VkBuffer buffer);
	static void DestroyCommandPool(VkCommandPool commandPool);
	static void DestroyDescriptorPool(VkDescriptorPool pool);
	static void DestroyDescriptorSetLayout(VkDescriptorSetLayout layout);
	static void DestroyFence(VkFence fence);
	static void DestroyFramebuffer(VkFramebuffer framebuffer);
	static void DestroyImage(VkImage image);
	static void DestroyImageView(VkImageView view);
	static void DestroyPipeline(VkPipeline pipeline);
	static void DestroyPipelineLayout(VkPipelineLayout layout);
	static void DestroyRenderPass(VkRenderPass renderPass);
	static void DestroySampler(VkSampler sampler);
	static void DestroyShaderModule(VkShaderModule shaderModule);
	static void DestroySemaphore(VkSemaphore semaphore);
	static void DestroySwapchain(VkSwapchainKHR swapchain);
	static void FreeCommandBuffers(VkCommandPool commandPool, uint32_t commandBufferCount, const VkCommandBuffer* commandBuffers);
	static void FreeDescriptorSets(VkDescriptorPool descriptorPool, uint32_t setsCount, const VkDescriptorSet* sets);
	static void FreeMemory(VkDeviceMemory memory);
	static void GetBufferMemoryRequirements(VkBuffer buffer, VkMemoryRequirements* pMemRequirements);
	static void GetImageMemoryRequirements(VkImage image, VkMemoryRequirements* pMemRequirements);
	static void GetSwapchainImages(VkSwapchainKHR swapchain, uint32_t* imageCount, VkImage* images);
	static void MapMemory(VkDeviceMemory deviceMemory, VkDeviceSize offset, VkDeviceSize size, void** data);
	static void UnmapMemory(VkDeviceMemory deviceMemory);
	static void UpdateDescriptorSets(uint32_t descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites, uint32_t descriptorCopyCount, const VkCopyDescriptorSet* pDescriptorCopies);
	static void WaitForFences(uint32_t fenceCount, const VkFence* fences);
	static void ResetFences(uint32_t fenceCount, const VkFence* fences);
	static void WaitIdle();
	static VkResult AcquireNextImage(VkSwapchainKHR swapchain, uint64_t timeout, VkSemaphore semaphore, VkFence fence, uint32_t* image);
private:
	static VkDevice device;
	static VkQueue graphicsQueue;
	static VkQueue presentQueue;

	static PhysicalDevice physicalDevice;
};

