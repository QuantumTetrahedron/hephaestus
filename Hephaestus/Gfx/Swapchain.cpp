#include "Swapchain.h"
#include "Device.h"
#include "Surface.h"
#include <cstdint>
#include <algorithm>
#include "CommandPool.h"
#include "Renderer.h"

void Swapchain::create(const Surface& surface)
{
	_create(surface);
	currentRecordingTarget = 0;
	createSemaphores();
}

void Swapchain::createImageViews() {
	imageViews.resize(swapChainImages.size());

	for (int i = 0; i < swapChainImages.size(); ++i) {
		VkImageViewCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = swapChainImages[i];
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = swapChainImageFormat;
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;
		
		Device::CreateImageView(&createInfo, &imageViews[i]);
	}
}

void Swapchain::transitionImages()
{
	auto command = CommandPool::beginSingleTimeCommands();

	transitionImages(command, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

	CommandPool::endSingleTimeCommands(command);
}

void Swapchain::transitionImages(VkCommandBuffer command, VkImageLayout newLayout) {
	for (int i = 0; i < swapChainImages.size(); i++) {
		VkImageMemoryBarrier barrier{};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = currentLayout;
		barrier.newLayout = newLayout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image = swapChainImages[i];
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = 0;

		VkPipelineStageFlags srcStage;
		VkPipelineStageFlags dstStage;

		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		dstStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

		vkCmdPipelineBarrier(command, srcStage, dstStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	}

	currentLayout = newLayout;
}

void Swapchain::cleanup()
{
	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
		Device::DestroySemaphore(renderFinishedSemaphores[i]);
		Device::DestroySemaphore(imageAvailableSemaphores[i]);
		Device::DestroyFence(inFlightFences[i]);
	}
	_cleanup();
}

VkResult Swapchain::acquireImage(uint32_t* i)
{
	Device::WaitForFences(1, &inFlightFences[currentFrame]);
	uint32_t imageIndex;
	VkResult res = Device::AcquireNextImage(swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);
	//if (res == VK_ERROR_OUT_OF_DATE_KHR) {
	//    *i = 0;
	//    return res;
	//}

	if (imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
		Device::WaitForFences(1, &imagesInFlight[imageIndex]);
	}
	imagesInFlight[imageIndex] = inFlightFences[currentFrame];
	Device::ResetFences(1, &inFlightFences[currentFrame]);
	*i = imageIndex;
	return res;
}

void Swapchain::submitCommands(const VkCommandBuffer* commandBuffers, uint32_t count)
{
	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = count;
	submitInfo.pCommandBuffers = commandBuffers;
	VkSemaphore signalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;
	Device::SubmitToGraphicsQueue(1, &submitInfo, inFlightFences[currentFrame]);
}

VkResult Swapchain::present(uint32_t imageIndex)
{
	VkSemaphore signalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
	VkPresentInfoKHR presentInfo{};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;
	//VkSwapchainKHR swapchains[] = { swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &swapChain;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr;
	auto res = Device::Present(&presentInfo);
	currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
	return res;
}

VkSwapchainKHR Swapchain::get() const
{
	return swapChain;
}

VkExtent2D Swapchain::getExtent() const
{
	return swapChainExtent;
}

VkFormat Swapchain::getImageFormat() const
{
	return swapChainImageFormat;
}

ImageData Swapchain::getImageData() const
{
	ImageData data;
	data.extent = getExtent();
	data.format = getImageFormat();
	data.isSwapchain = true;
	data.name = "swapchain";
	return data;
}

std::vector<VkImage> Swapchain::getImages() const
{
	return swapChainImages;
}

void Swapchain::Display(VkCommandBuffer command, std::shared_ptr<Image> image, uint32_t index)
{
	image->transitionLayout(command, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
	
	transitionImages(command, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	image->copyTo(command, getImages()[index], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, true);
	transitionImages(command, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

	/*std::string renderPassQuad = "quad_swapcain";

	VkRenderPassBeginInfo renderPassInfo{};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = getRenderPasses()[index];

	// texture as output, instead of swapchain
	renderPassInfo.framebuffer = getFramebuffers()[index];
	renderPassInfo.renderArea.offset = { 0,0 };
	renderPassInfo.renderArea.extent = getExtent();

	std::array<VkClearValue, 1> clearValues;
	clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
	renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(command, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	auto shader = ResourceManager::GetShader(shaderName);

	shader->Use(_id, command);
	shader->BindDescriptors(_id, command, 0, 1, &Renderer::globalDescriptorSet);

	auto model = ResourceManager::GetModel(quadName);

	for (auto& mesh : model->meshes) {
		mesh.Bind(_id, command, *shader, 0, Renderer::vertexIndexBuffer);
		mesh.Draw(command);
	}

	vkCmdEndRenderPass(command);*/
}

void Swapchain::createSemaphores()
{
	imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
	imagesInFlight.resize(swapChainImages.size(), VK_NULL_HANDLE);

	VkSemaphoreCreateInfo semaphoreInfo{};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo{};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
		Device::CreateSemaphore(&semaphoreInfo, &imageAvailableSemaphores[i]);
		Device::CreateSemaphore(&semaphoreInfo, &renderFinishedSemaphores[i]);
		Device::CreateFence(&fenceInfo, &inFlightFences[i]);
	}
}

VkSurfaceFormatKHR Swapchain::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) const
{
	for (const auto& availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLORSPACE_SRGB_NONLINEAR_KHR) {
			//std::cout << "format found" << std::endl;
			return availableFormat;
		}
	}
	//std::cout << "format not found" << std::endl;
	return availableFormats[0];
}

VkPresentModeKHR Swapchain::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) const
{
	for (const auto& availablePresentMode : availablePresentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			//std::cout << "mailbox" << std::endl;
			return availablePresentMode;
		}
	}

	//std::cout << "fifo" << std::endl;
	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D Swapchain::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, uint32_t w, uint32_t h) const
{
	if (capabilities.currentExtent.width != UINT32_MAX) {
		return capabilities.currentExtent;
	}
	else {
		VkExtent2D actualExtent = { w, h };

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}
}

void Swapchain::recreate(const Surface& surface)
{
	_cleanup();
	_create(surface);
}

void Swapchain::_create(const Surface& surface)
{
	SwapChainSupportDetails swapchainSupport = Device::getPhysicalDevice().getSwapChainSupport(surface);

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapchainSupport.formats);
	VkPresentModeKHR presentMode = chooseSwapPresentMode(swapchainSupport.presentModes);
	VkExtent2D extent = chooseSwapExtent(swapchainSupport.capabilities, surface.width, surface.height);

	uint32_t imageCount = swapchainSupport.capabilities.minImageCount + 1;
	if (swapchainSupport.capabilities.maxImageCount > 0 && imageCount > swapchainSupport.capabilities.maxImageCount) {
		imageCount = swapchainSupport.capabilities.maxImageCount;
	}

	VkImageUsageFlags usage;
	if (swapchainSupport.capabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT) {
		usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	}
	else {
		std::cerr << "Swapchain images do not support transfer dst usage" << std::endl;
		usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	}

	VkSwapchainCreateInfoKHR createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface.get();
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = usage;

	QueueFamilyIndices indices = Device::getPhysicalDevice().getQueueFamilies();
	uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };
	if (indices.graphicsFamily != indices.presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}
	createInfo.preTransform = swapchainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE;

	Device::CreateSwapchain(&createInfo, &swapChain);

	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;

	Device::GetSwapchainImages(swapChain, &imageCount, nullptr);
	swapChainImages.resize(imageCount);
	Device::GetSwapchainImages(swapChain, &imageCount, swapChainImages.data());

	createImageViews();
	transitionImages();
}

void Swapchain::_cleanup()
{
	for (auto imageView : imageViews) {
		Device::DestroyImageView(imageView);
	}
	Device::DestroySwapchain(swapChain);
}

