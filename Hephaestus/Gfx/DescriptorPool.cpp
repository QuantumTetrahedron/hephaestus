#include "DescriptorPool.h"
#include "Device.h"
#include <array>

void DescriptorPool::create(VkDescriptorType type, uint32_t setsCount, uint32_t descriptorCount)
{
	VkDescriptorPoolSize poolSize{};
	poolSize.type = type;
	poolSize.descriptorCount = descriptorCount;

	VkDescriptorPoolCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	createInfo.poolSizeCount = 1;
	createInfo.pPoolSizes = &poolSize;
	createInfo.maxSets = setsCount;

	Device::CreateDescriptorPool(&createInfo, &pool);
}

void DescriptorPool::cleanup()
{
	Device::DestroyDescriptorPool(pool);
}

void DescriptorPool::Allocate(uint32_t count, const VkDescriptorSetLayout* layouts, VkDescriptorSet* sets)
{
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = pool;
	allocInfo.descriptorSetCount = count;
	allocInfo.pSetLayouts = layouts;

	Device::AllocateDescriptorSets(&allocInfo, sets);
}

void DescriptorPool::Free(uint32_t descriptorCount, const VkDescriptorSet* sets)
{
	Device::FreeDescriptorSets(pool, descriptorCount, sets);
}
