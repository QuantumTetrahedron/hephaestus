#pragma once
#include "vk.h"

class Instance;
class Window;

class Surface
{
public:
	void create(const Instance& instance, const Window& window);
	void cleanup(const Instance& instance);

	VkSurfaceKHR get() const;

	uint32_t width, height;
private:
	VkSurfaceKHR surface;
};

