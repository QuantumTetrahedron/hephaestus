#pragma once
#include <vector>
#include "GraphicsPipeline.h"
#include "Image.h"
#include "Framebuffer.h"
#include "RendererData.h"

class Surface;

class Swapchain
{
public:
	void create(const Surface& surface);
	void cleanup();

	VkResult acquireImage(uint32_t* imageIndex);
	void submitCommands(const VkCommandBuffer* commandBuffers, uint32_t count);
	VkResult present(uint32_t imageIndex);

	VkSwapchainKHR get() const;
	VkExtent2D getExtent() const;
	VkFormat getImageFormat() const;
	ImageData getImageData() const;

	void recreate(const Surface& surface);
	std::vector<VkImage> getImages() const;

	void Display(VkCommandBuffer command, std::shared_ptr<Image> image, uint32_t index);

	uint32_t currentRecordingTarget;
private:
	void createImageViews();

	void transitionImages();
	void transitionImages(VkCommandBuffer command, VkImageLayout newLayout);
	VkImageLayout currentLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	VkSwapchainKHR swapChain;
	std::vector<VkImage> swapChainImages;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;

	std::vector<VkImageView> imageViews;

	void _create(const Surface& surface);
	void _cleanup();

	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) const;
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) const;
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, uint32_t w, uint32_t h) const;

	const int MAX_FRAMES_IN_FLIGHT = 2;
	void createSemaphores();
	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences;
	std::vector<VkFence> imagesInFlight;
	size_t currentFrame = 0;
};

