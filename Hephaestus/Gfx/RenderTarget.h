#pragma once

#include "Framebuffer.h"
#include "Image.h"
#include "RendererData.h"

class RenderTarget
{
public:
	void create(const std::vector<ImageData>& imagesData, bool hasDepth, VkExtent2D extent);
	void cleanup();

	VkExtent2D getExtent() const;

	Framebuffer getFramebuffer(uint32_t rpId) const;

	void createFramebuffer(uint32_t rpId, const RenderPass& renderPass);

	void transitionLayout(VkImageLayout newLayout);

	std::vector<VkImageView> colorImageViews;
	std::optional<VkImageView> depthImageView;
private:
	//std::vector<VkFormat> formats;

	// TODO: Render target cant own images
	//std::vector<Image> images;
	std::vector<std::weak_ptr<Image>> colorImages;
	std::optional<std::weak_ptr<Image>> depthImage;

	VkExtent2D extent;

	std::map<uint32_t, Framebuffer> framebuffers;
};