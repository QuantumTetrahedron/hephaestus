#pragma once
#include <vector>
#include "vk.h"

class CommandPool
{
public:
	static void create();
	static void cleanup();

	static void Allocate(uint32_t count, VkCommandBuffer* commands);
	static void Free(uint32_t count, VkCommandBuffer* commands);

	static VkCommandBuffer beginSingleTimeCommands();
	static void endSingleTimeCommands(VkCommandBuffer command);
private:
	static VkCommandPool commandPool;
};

