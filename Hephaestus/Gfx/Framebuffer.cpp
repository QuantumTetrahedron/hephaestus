#include "Framebuffer.h"
#include "RenderPass.h"
#include <array>
#include "Device.h"

void Framebuffer::create(const std::vector<VkImageView>& colorImageViews, std::optional<VkImageView> depthImageView, const RenderPass& renderPass, VkExtent2D ext)
{
	colorAttachmentsCount = colorImageViews.size();
	hasDepth = depthImageView.has_value();

	std::vector<VkImageView> imageViews{ colorImageViews };
	if (depthImageView.has_value()) {
		imageViews.push_back(depthImageView.value());
	}

	VkFramebufferCreateInfo framebufferInfo{};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.renderPass = renderPass.get();
	framebufferInfo.attachmentCount = static_cast<uint32_t>(imageViews.size());
	framebufferInfo.pAttachments = imageViews.data();
	framebufferInfo.width = ext.width;
	framebufferInfo.height = ext.height;
	framebufferInfo.layers = 1;

	Device::CreateFramebuffer(&framebufferInfo, &framebuffer);

	extent = ext;
}

void Framebuffer::destroy()
{
	Device::DestroyFramebuffer(framebuffer);
}
