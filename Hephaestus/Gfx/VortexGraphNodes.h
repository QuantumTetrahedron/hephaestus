#pragma once

#include "Renderer.h"
#include <functional>
#include <algorithm>
#include <set>

class VGNode {
public:
	VGNode(int id);

	bool is(int id);
	void Connect(VGNode* other, const std::string& port, const std::string& otherPort);

	virtual ~VGNode() = default;
protected:
	int _id;

	void AddPort(const std::string& name);
	void ConnectToPort(const std::string& name, VGNode* node);

	template<typename T>
	std::vector<T*> GetNodes(const std::string& portName) const;
	template<typename T>
	T* GetNode(const std::string& portName) const;
private:
	const std::vector<VGNode*>& GetPort(const std::string& name) const;
	std::map<std::string, std::vector<VGNode*>> _ports;
};

template<typename T>
inline std::vector<T*> VGNode::GetNodes(const std::string& portName) const
{
	auto& vec = GetPort(portName);
	std::vector<T*> out;
	std::transform(std::begin(vec), std::end(vec), std::back_inserter(out), [](VGNode* n) {return dynamic_cast<T*>(n); });
	return out;
}

template<typename T>
inline T* VGNode::GetNode(const std::string& portName) const
{
	auto& vec = GetPort(portName);
	if (vec.empty()) {
		return nullptr;
	}
	return dynamic_cast<T*>(vec[0]);
}

template<typename T>
class VGGetter {
public:
	virtual T Get() = 0;
};

class VGExecutable {
public:
	using nodeFunc = std::function<void(VGExecutable*, VkCommandBuffer)>;
	struct RunConfig {
		VkCommandBuffer command;
		bool broadcast;
		std::set<VGExecutable*> visited;
	};

	virtual void Run(nodeFunc func, RunConfig& config) = 0;

	virtual void Execute(VkCommandBuffer buffer) {}
	void Cleanup();
	void Initialize();
	virtual void Reset() {}
protected:
	bool active;
	virtual void Init() {}
	virtual void Clean() {}

	bool Visit(nodeFunc func, RunConfig& config);
};

// -------------------------------------

class StartNode : public VGNode, public VGExecutable {
public:
	StartNode(int id);
	void Run(nodeFunc func, RunConfig& config);
};

class RenderPassNode : public VGNode, public VGExecutable {
public:
	RenderPassNode(int id);
	void Run(nodeFunc func, RunConfig& config);
	void Execute(VkCommandBuffer buffer);
	void Init();
	void Clean();
	void Reset();
private:
	std::vector<GroupData> inputGroups;
	std::vector<ImageData> inputImages;
	std::vector<VkImageView> inputImageViews;
};

class IfNode : public VGNode, public VGExecutable {
public:
	IfNode(int id);

	void Run(nodeFunc func, RunConfig& config);
};

class WhileNode : public VGNode, public VGExecutable {
public:
	WhileNode(int id);

	void Run(nodeFunc func, RunConfig& config);
};

template<typename IN, typename OUT>
class OperatorNode1 : public VGNode, public VGGetter<OUT> {
public:
	OperatorNode1(std::function<OUT(IN)> func, int id) : VGNode(id), function(func) {
		AddPort("in");
		AddPort("out");
	}

	OUT Get() {
		VGGetter<IN>* in = GetNode<VGGetter<IN>>("in");
		IN arg = in != nullptr ? in->Get() : IN();
		OUT result = function(arg);
		return result;
	}

private:
	std::function<OUT(IN)> function;
};

template<typename IN1, typename IN2, typename OUT>
class OperatorNode2 : public VGNode, public VGGetter<OUT> {
public:
	OperatorNode2(std::function<OUT(IN1, IN2)> func, int id) : VGNode(id), function(func) {
		AddPort("in1");
		AddPort("in2");
		AddPort("out");
	}

	OUT Get() {
		VGGetter<IN1>* in1 = GetNode<VGGetter<IN1>>("in1");
		VGGetter<IN2>* in2 = GetNode<VGGetter<IN2>>("in2");
		
		IN1 arg1 = in1 != nullptr ? in1->Get() : IN1();
		IN2 arg2 = in2 != nullptr ? in2->Get() : IN2();

		OUT result = function(arg1, arg2);
		return result;
	}
private:
	std::function<OUT(IN1, IN2)> function;
};

template<typename T>
class VarGetterNode : public VGNode, public VGGetter<T> {
public:
	VarGetterNode(const std::string& name, int id) : VGNode(id), name(name) {
		AddPort("get");
	}
	
	T Get() {
		T out;
		if (!Renderer::GetVarCurrent(name, &out)) {
			std::cerr << "Variable " << name << " not found!" << std::endl;
		}
		return out;
	}
protected:
	std::string name;
};

template<typename T>
class VarSelectNode : public VGNode, public VGGetter<T> {
public:
	VarSelectNode(int id) : VGNode(id) {
		AddPort("choice");
		AddPort("opt1");
		AddPort("opt2");
		AddPort("opt3");
		AddPort("opt4");
		AddPort("out");
	}

	T Get() {
		T out;
		int choice;
		auto choiceNode = GetNode<VGGetter<int>>("choice");
		if (choiceNode) {
			choice = choiceNode->Get();
		}
		else {
			choice = 0;
		}
		
		std::string name = "opt";
		std::stringstream ss;
		ss << (choice + 1);
		name += ss.str();
		auto chosenNode = GetNode<VGGetter<T>>(name);
		if (chosenNode) {
			out = chosenNode->Get();
		}
		else {
			std::cerr << "No option in select node" << std::endl;
		}

		return out;
	}
};

template<typename T>
class VarSetterNode : public VGNode, public VGGetter<T>, public VGExecutable {
public:
	VarSetterNode(const std::string& name, int id) : VGNode(id), name(name) {
		AddPort("get");
		AddPort("set");
		AddPort("prev");
		AddPort("next");
	}

	T Get() {
		T out;
		if (!Renderer::GetVarCurrent(name, &out)) {
			std::cerr << "Variable " << name << " not found!" << std::endl;
		}
		return out;
	}

	void Run(nodeFunc func, RunConfig& config) {
		if (!Visit(func, config)) return;
		auto next = GetNode<VGExecutable>("next");
		if (next) next->Run(func, config);
	}

	void Execute(VkCommandBuffer buffer) {
		VGGetter<T>* set = GetNode<VGGetter<T>>("set");
		T setValue = set != nullptr ? set->Get() : T();
		Renderer::SetVarCurrent(name, setValue);
	}
private:
	std::string name;
};

class ImageNode : public VarGetterNode<ImageData> {
public:
	ImageNode(const std::string& name, int id);
};

class SwapchainNode : public VGNode, public VGGetter<ImageData> {
public:
	SwapchainNode(int id);
	ImageData Get();
};

class VGNodeFactory {
public:
	static VGNode* LoadNode(const std::string& code);

private:
	static VGNode* LoadOperatorNode(const std::vector<std::string>& split, int id);
	static VGNode* LoadGetterNode(const std::vector<std::string>& split, int id);
	static VGNode* LoadSetterNode(const std::vector<std::string>& split, int id);
	static VGNode* LoadSelectNode(const std::vector<std::string>& split, int id);

	template<typename IN1, typename IN2, typename OUT>
	static VGNode* LoadOperatorNodeT(const std::string& operatorCode, int id);
};

template<typename IN1, typename IN2, typename OUT>
VGNode* VGNodeFactory::LoadOperatorNodeT(const std::string& operatorCode, int id)
{
	if (operatorCode == "add") {
		return new OperatorNode2<IN1, IN2, OUT>([](IN1 a, IN2 b) {return a + b; }, id);
	}
	if (operatorCode == "sub") {
		return new OperatorNode2<IN1, IN2, OUT>([](IN1 a, IN2 b) {return a - b; }, id);
	}
	if (operatorCode == "mul") {
		return new OperatorNode2<IN1, IN2, OUT>([](IN1 a, IN2 b) {return a - b; }, id);
	}
	if (operatorCode == "div") {
		return new OperatorNode2<IN1, IN2, OUT>([](IN1 a, IN2 b) {return a - b; }, id);
	}
	if (operatorCode == "eq") {
		return new OperatorNode2<IN1, IN2, bool>([](IN1 a, IN2 b) {return a == b; }, id);
	}
	if (operatorCode == "neq") {
		return new OperatorNode2<IN1, IN2, bool>([](IN1 a, IN2 b) {return a != b; }, id);
	}
	if (operatorCode == "gt") {
		return new OperatorNode2<IN1, IN2, bool>([](IN1 a, IN2 b) {return a > b; }, id);
	}
	if (operatorCode == "geq") {
		return new OperatorNode2<IN1, IN2, bool>([](IN1 a, IN2 b) {return a >= b; }, id);
	}
	if (operatorCode == "lt") {
		return new OperatorNode2<IN1, IN2, bool>([](IN1 a, IN2 b) {return a < b; }, id);
	}
	if (operatorCode == "leq") {
		return new OperatorNode2<IN1, IN2, bool>([](IN1 a, IN2 b) {return a <= b; }, id);
	}
	return nullptr;
}
