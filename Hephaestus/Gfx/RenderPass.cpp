#include "RenderPass.h"
#include "Device.h"
#include <array>
#include "../Resources/ResourceManager.h"
#include "Renderer.h"
#include "Framebuffer.h"
#include "../Core/CameraComponent.h"

void RenderPass::create(uint32_t id, std::vector<Config>& configs, std::optional<Config> depthConfig, std::shared_ptr<Shader> shader)
{
	_id = id;
	_shader = shader;

	std::vector<VkAttachmentDescription> attachments;
	std::vector<VkAttachmentReference> colorAttachmentRefs;
	for (int i = 0; i < configs.size(); ++i) {
		Config c = configs[i];
		VkAttachmentDescription colorAttachment{};
		colorAttachment.format = c.format;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = c.loadOp;
		colorAttachment.storeOp = c.storeOp;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = c.initialLayout;
		colorAttachment.finalLayout = c.finalLayout;
		attachments.push_back(colorAttachment);

		VkAttachmentReference colorAttachmentRef{};
		colorAttachmentRef.attachment = i;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		colorAttachmentRefs.push_back(colorAttachmentRef);
	}
	colorAttachmentsCount = colorAttachmentRefs.size();

	std::optional<VkAttachmentReference> depthAttachmentRef;
	hasDepthAttachment = false;
	if (depthConfig.has_value()) {
		Config c = depthConfig.value();
		VkAttachmentDescription depthAttachment{};
		depthAttachment.format = c.format;
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = c.loadOp;
		depthAttachment.storeOp = c.storeOp;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = c.initialLayout;
		depthAttachment.finalLayout = c.finalLayout;
		attachments.push_back(depthAttachment);

		VkAttachmentReference ref{};
		ref.attachment = attachments.size() - 1;
		ref.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		depthAttachmentRef = ref;
		hasDepthAttachment = true;
	}

	VkSubpassDescription subpass{};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = colorAttachmentRefs.size();
	subpass.pColorAttachments = colorAttachmentRefs.data();
	subpass.pDepthStencilAttachment = depthAttachmentRef.has_value() ? &depthAttachmentRef.value() : nullptr;

	/*VkSubpassDependency dependency{};
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	dependency.srcAccessMask = 0;
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	dependency.dstSubpass = 0;

	VkSubpassDependency dependency2{};
	dependency2.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency2.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependency2.srcSubpass = 0;
	dependency2.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency2.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
	dependency2.dstSubpass = VK_SUBPASS_EXTERNAL;*/

	VkSubpassDependency dependency{};
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependency.srcSubpass = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependency.dstAccessMask = 0;
	dependency.dstSubpass = VK_SUBPASS_EXTERNAL;

	VkSubpassDependency dependency2{};
	dependency2.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependency2.srcAccessMask = 0;
	dependency2.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency2.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency2.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
	dependency2.dstSubpass = 0;

	std::vector<VkSubpassDependency> deps = { dependency, dependency2 };

	VkRenderPassCreateInfo renderPassInfo{};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = deps.size();
	renderPassInfo.pDependencies = deps.data();

	Device::CreateRenderPass(&renderPassInfo, &renderPass);
}

void RenderPass::cleanup()
{
	Device::DestroyRenderPass(renderPass);
}

RenderPass::Config RenderPass::createConfig(VkFormat imageFormat, bool clear, bool isDepth, bool isSwapchain) {
	RenderPass::Config config;

	VkImageLayout imageTargetLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	VkImageLayout swapchainTargetLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	VkImageLayout targetLayout = isSwapchain ? swapchainTargetLayout : imageTargetLayout;
	targetLayout = isDepth ? VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : targetLayout;

	config.format = imageFormat;
	config.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	config.finalLayout = targetLayout;

	if (clear) {
		config.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		config.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	}
	else {
		config.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		config.initialLayout = targetLayout;
	}

	return config;
}

void RenderPass::recordCommands(VkCommandBuffer command, Framebuffer& target, std::vector<std::shared_ptr<RenderComponent>>& components, glm::vec3 clearColor)
{
	VkRenderPassBeginInfo renderPassInfo{};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = renderPass;

	renderPassInfo.framebuffer = target.framebuffer;
	renderPassInfo.renderArea.offset = { 0,0 };
	renderPassInfo.renderArea.extent = target.extent;

	std::vector<VkClearValue> clearValues;
	for (int i = 0; i < target.colorAttachmentsCount; ++i) {
		VkClearValue clear{};
		clear.color = { clearColor.r, clearColor.g, clearColor.b, 1.0f };
		clearValues.push_back(clear);
	}
	if (target.hasDepth) {
		VkClearValue clear{};
		clear.depthStencil = { 1.0f, 0 };
		clearValues.push_back(clear);
	}

	renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(command, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	auto shader = _shader.lock();
	shader->Use(_id, command);
	shader->BindDescriptors(_id, command, 0, 1, &Renderer::globalDescriptorSet);

	for (auto& p : ResourceManager::models) {
		for (auto& mesh : p.second->meshes) {
			mesh.Bind(_id, command, *shader, Renderer::vertexIndexBuffer);

			for (size_t r = 0; r < components.size(); ++r) {
				auto& rc = components[r];
				if (rc->model == p.second){
					shader->BindDescriptors(_id, command, 1, 1, &Renderer::objectDescriptorSets[rc->descriptorSetIndex]);
					mesh.Draw(command);
				}
			}
		}
	}

	vkCmdEndRenderPass(command);
}

void RenderPass::recordPostprocessCommands(VkCommandBuffer command, Framebuffer& target, std::string quadName, std::string shaderName)
{
	VkRenderPassBeginInfo renderPassInfo{};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = renderPass;

	// texture as output, instead of swapchain
	renderPassInfo.framebuffer = target.framebuffer; // swapchain.getFramebuffers()[i];
	renderPassInfo.renderArea.offset = { 0,0 };
	renderPassInfo.renderArea.extent = target.extent; // swapchain.getExtent();

	std::vector<VkClearValue> clearValues;
	for (int i = 0; i < target.colorAttachmentsCount; ++i) {
		VkClearValue clear{};
		clear.color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues.push_back(clear);
	}
	if (target.hasDepth) {
		VkClearValue clear{};
		clear.depthStencil = { 1.0f, 0 };
		clearValues.push_back(clear);
	}

	renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(command, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	auto shader = ResourceManager::GetShader(shaderName);

	shader->Use(_id, command);
	shader->BindDescriptors(_id, command, 0, 1, &Renderer::globalDescriptorSet);

	auto model = ResourceManager::GetModel(quadName);

	for (auto& mesh : model->meshes) {
		mesh.Bind(_id, command, *shader, Renderer::vertexIndexBuffer);
		mesh.Draw(command);
	}

	vkCmdEndRenderPass(command);
}

VkRenderPass RenderPass::get() const
{
	return renderPass;
}