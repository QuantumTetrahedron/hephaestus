#include "Surface.h"
#include "Instance.h"
#include "../Input/Window.h"

void Surface::create(const Instance& instance, const Window& window) {
    if (glfwCreateWindowSurface(instance.get(), window.get(), nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("failed to create window surface");
    }
    width = window.width;
    height = window.height;
}

void Surface::cleanup(const Instance& instance) {
    vkDestroySurfaceKHR(instance.get(), surface, nullptr);
}

VkSurfaceKHR Surface::get() const
{
    return surface;
}
