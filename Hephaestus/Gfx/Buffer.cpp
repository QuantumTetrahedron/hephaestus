#include "Buffer.h"
#include "CommandPool.h"
#include "Device.h"

VkBuffer Buffer::get() const
{
	return buffer;
}

VkDeviceMemory Buffer::getMemory() const
{
	return memory;
}

void Buffer::copy(Buffer& src, VkDeviceSize size)
{
	auto command = CommandPool::beginSingleTimeCommands();

	VkBufferCopy copyRegion{};
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = size;
	vkCmdCopyBuffer(command, src.get(), buffer, 1, &copyRegion);

	CommandPool::endSingleTimeCommands(command);
}

void Buffer::create(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties)
{
	if (size == 0) {
		return;
	}

	VkBufferCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	createInfo.size = size;
	createInfo.usage = usage;
	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	Device::CreateBuffer(&createInfo, &buffer);

	VkMemoryRequirements memRequirements;
	Device::GetBufferMemoryRequirements(buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = Device::findMemoryType(memRequirements.memoryTypeBits, properties);
	Device::AllocateMemory(&allocInfo, &memory);

	Device::BindBufferMemory(buffer, memory, 0);
}

void Buffer::cleanup()
{
	Device::DestroyBuffer(buffer);
	Device::FreeMemory(memory);
}
