#include "Device.h"
#include <set>
#include <stdexcept>

VkDevice Device::device;
VkQueue Device::graphicsQueue;
VkQueue Device::presentQueue;
PhysicalDevice Device::physicalDevice;

void Device::create(const Instance& instance, const Surface& surface) {
    physicalDevice.pick(instance, surface);

    QueueFamilyIndices indices = physicalDevice.getQueueFamilies();

    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };
    float queuePriority = 1.0f;

    for (uint32_t queueFamily : uniqueQueueFamilies) {
        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.samplerAnisotropy = VK_TRUE;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(physicalDevice.deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = physicalDevice.deviceExtensions.data();
    /*if (debug) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(instance.debugManager.validationLayers.size());
        createInfo.ppEnabledLayerNames = instance.debugManager.validationLayers.data();
    }
    else {*/
        createInfo.enabledLayerCount = 0;
    //}

    if (vkCreateDevice(physicalDevice.get(), &createInfo, nullptr, &device) != VK_SUCCESS) {
        throw std::runtime_error("failed to create logical device");
    }

    vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);
    vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &presentQueue);
}

void Device::cleanup() {
	vkDestroyDevice(device, nullptr);
}

VkQueue Device::getGraphicsQueue()
{
    return graphicsQueue;
}

VkQueue Device::getPresentQueue() {
    return presentQueue;
}
void Device::SubmitToGraphicsQueue(uint32_t submitCount, const VkSubmitInfo* infos, VkFence fence)
{
    if (vkQueueSubmit(graphicsQueue, submitCount, infos, fence) != VK_SUCCESS) {
        throw std::runtime_error("failed to submit draw command buffer");
    }
}
VkResult Device::Present(const VkPresentInfoKHR* presentInfo)
{
    return vkQueuePresentKHR(presentQueue, presentInfo);
}

uint32_t Device::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties memProperties;
    physicalDevice.GetMemoryProperties(&memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i) {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    throw std::runtime_error("failed to find suitable memory type");
}

VkFormat Device::findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(physicalDevice.get(), format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            return format;
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
            return format;
        }
    }
    throw std::runtime_error("failed to find supported format");
}

VkFormat Device::findDepthFormat()
{
    return findSupportedFormat({ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT }, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

PhysicalDevice Device::getPhysicalDevice()
{
    return physicalDevice;
}

void Device::AllocateCommandBuffers(const VkCommandBufferAllocateInfo* allocInfo, VkCommandBuffer* commandBuffers)
{
    if (vkAllocateCommandBuffers(device, allocInfo, commandBuffers) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate command buffers");
    }
}

void Device::AllocateDescriptorSets(const VkDescriptorSetAllocateInfo* allocInfo, VkDescriptorSet* descriptorSets)
{
    if (vkAllocateDescriptorSets(device, allocInfo, descriptorSets) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate descriptor sets");
    }
}

void Device::AllocateMemory(const VkMemoryAllocateInfo* allocInfo, VkDeviceMemory* pDeviceMemory)
{
    if (vkAllocateMemory(device, allocInfo, nullptr, pDeviceMemory) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate buffer memory");
    }
}

void Device::BindBufferMemory(VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize memoryOffset)
{
    vkBindBufferMemory(device, buffer, memory, 0);
}

void Device::BindImageMemory(VkImage image, VkDeviceMemory memory, VkDeviceSize memoryOffset)
{
    vkBindImageMemory(device, image, memory, memoryOffset);
}

void Device::CreateBuffer(const VkBufferCreateInfo* createInfo, VkBuffer* pBuffer)
{
    if (vkCreateBuffer(device, createInfo, nullptr, pBuffer) != VK_SUCCESS) {
        throw std::runtime_error("failed to create buffer");
    }
}

void Device::CreateCommandPool(const VkCommandPoolCreateInfo* createInfo, VkCommandPool* commandPool)
{
    if (vkCreateCommandPool(device, createInfo, nullptr, commandPool) != VK_SUCCESS) {
        throw std::runtime_error("failed to create command pool");
    }
}

void Device::CreateDescriptorPool(const VkDescriptorPoolCreateInfo* createInfo, VkDescriptorPool* pool)
{
    if (vkCreateDescriptorPool(device, createInfo, nullptr, pool) != VK_SUCCESS) {
        throw std::runtime_error("failed to create descriptor pool");
    }
}

void Device::CreateDescriptorSetLayout(const VkDescriptorSetLayoutCreateInfo* createInfo, VkDescriptorSetLayout* layout)
{
    if (vkCreateDescriptorSetLayout(device, createInfo, nullptr, layout) != VK_SUCCESS) {
        throw std::runtime_error("failed to create descriptor set layout");
    }
}

void Device::CreateFence(const VkFenceCreateInfo* createInfo, VkFence* fence)
{
    if (vkCreateFence(device, createInfo, nullptr, fence) != VK_SUCCESS) {
        throw std::runtime_error("failed to create fence");
    }
}

void Device::CreateFramebuffer(const VkFramebufferCreateInfo* createInfo, VkFramebuffer* framebuffer)
{
    if (vkCreateFramebuffer(device, createInfo, nullptr, framebuffer) != VK_SUCCESS) {
        throw std::runtime_error("failed to create framebuffer");
    }
}

void Device::CreateGraphicsPipelines(uint32_t createInfoCount, const VkGraphicsPipelineCreateInfo* createInfos, VkPipeline* pipelines)
{
    if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, createInfoCount, createInfos, nullptr, pipelines) != VK_SUCCESS) {
        throw std::runtime_error("failed to create graphics pipeline");
    }
}

void Device::CreateImage(const VkImageCreateInfo* createInfo, VkImage* image)
{
    if (vkCreateImage(device, createInfo, nullptr, image) != VK_SUCCESS) {
        throw std::runtime_error("failed to create image");
    }
}

void Device::CreateImageView(const VkImageViewCreateInfo* createInfo, VkImageView* view)
{
    if (vkCreateImageView(device, createInfo, nullptr, view) != VK_SUCCESS) {
        throw std::runtime_error("failed to create image views!");
    }
}

void Device::CreatePipelineLayout(const VkPipelineLayoutCreateInfo* createInfo, VkPipelineLayout* layout)
{
    if (vkCreatePipelineLayout(device, createInfo, nullptr, layout) != VK_SUCCESS) {
        throw std::runtime_error("failed to create pipeline layout");
    }
}

void Device::CreateRenderPass(const VkRenderPassCreateInfo* createInfo, VkRenderPass* renderPass)
{
    if (vkCreateRenderPass(device, createInfo, nullptr, renderPass) != VK_SUCCESS) {
        throw std::runtime_error("failed to create render pass");
    }
}

void Device::CreateSampler(const VkSamplerCreateInfo* createInfo, VkSampler* sampler)
{
    if (vkCreateSampler(device, createInfo, nullptr, sampler) != VK_SUCCESS) {
        throw std::runtime_error("failed to create sampler");
    }
}

void Device::CreateSemaphore(const VkSemaphoreCreateInfo* createInfo, VkSemaphore* semaphore)
{
    if (vkCreateSemaphore(device, createInfo, nullptr, semaphore) != VK_SUCCESS) {
        throw std::runtime_error("failed to create semaphore");
    }
}

void Device::CreateShaderModule(const VkShaderModuleCreateInfo* createInfo, VkShaderModule* shaderModule)
{
    if (vkCreateShaderModule(device, createInfo, nullptr, shaderModule) != VK_SUCCESS) {
        throw std::runtime_error("failed to create shader module");
    }
}

void Device::CreateSwapchain(const VkSwapchainCreateInfoKHR* createInfo, VkSwapchainKHR* swapchain)
{
    if (vkCreateSwapchainKHR(device, createInfo, nullptr, swapchain) != VK_SUCCESS) {
        throw std::runtime_error("failed to create swapchain");
    }
}

void Device::DestroyBuffer(VkBuffer buffer)
{
    vkDestroyBuffer(device, buffer, nullptr);
}

void Device::DestroyCommandPool(VkCommandPool commandPool)
{
    vkDestroyCommandPool(device, commandPool, nullptr);
}

void Device::DestroyDescriptorPool(VkDescriptorPool pool)
{
    vkDestroyDescriptorPool(device, pool, nullptr);
}

void Device::DestroyDescriptorSetLayout(VkDescriptorSetLayout layout)
{
    vkDestroyDescriptorSetLayout(device, layout, nullptr);
}

void Device::DestroyFence(VkFence fence)
{
    vkDestroyFence(device, fence, nullptr);
}

void Device::DestroyFramebuffer(VkFramebuffer framebuffer)
{
    vkDestroyFramebuffer(device, framebuffer, nullptr);
}

void Device::DestroyImage(VkImage image)
{
    vkDestroyImage(device, image, nullptr);
}

void Device::DestroyImageView(VkImageView view)
{
    vkDestroyImageView(device, view, nullptr);
}

void Device::DestroyPipeline(VkPipeline pipeline)
{
    vkDestroyPipeline(device, pipeline, nullptr);
}

void Device::DestroyPipelineLayout(VkPipelineLayout layout)
{
    vkDestroyPipelineLayout(device, layout, nullptr);
}

void Device::DestroyRenderPass(VkRenderPass renderPass)
{
    vkDestroyRenderPass(device, renderPass, nullptr);
}

void Device::DestroySampler(VkSampler sampler)
{
    vkDestroySampler(device, sampler, nullptr);
}

void Device::DestroyShaderModule(VkShaderModule shaderModule)
{
    vkDestroyShaderModule(device, shaderModule, nullptr);
}

void Device::DestroySemaphore(VkSemaphore semaphore)
{
    vkDestroySemaphore(device, semaphore, nullptr);
}

void Device::DestroySwapchain(VkSwapchainKHR swapchain)
{
    vkDestroySwapchainKHR(device, swapchain, nullptr);
}

void Device::FreeCommandBuffers(VkCommandPool commandPool, uint32_t commandBufferCount, const VkCommandBuffer* commandBuffers)
{
    vkFreeCommandBuffers(device, commandPool, commandBufferCount, commandBuffers);
}

void Device::FreeDescriptorSets(VkDescriptorPool descriptorPool, uint32_t setsCount, const VkDescriptorSet* sets)
{
    vkFreeDescriptorSets(device, descriptorPool, setsCount, sets);
}

void Device::FreeMemory(VkDeviceMemory memory)
{
    vkFreeMemory(device, memory, nullptr);
}

void Device::GetBufferMemoryRequirements(VkBuffer buffer, VkMemoryRequirements* pMemRequirements)
{
    vkGetBufferMemoryRequirements(device, buffer, pMemRequirements);
}

void Device::GetImageMemoryRequirements(VkImage image, VkMemoryRequirements* pMemRequirements)
{
    vkGetImageMemoryRequirements(device, image, pMemRequirements);
}

void Device::GetSwapchainImages(VkSwapchainKHR swapchain, uint32_t* imageCount, VkImage* images)
{
    vkGetSwapchainImagesKHR(device, swapchain, imageCount, images);
}

void Device::MapMemory(VkDeviceMemory deviceMemory, VkDeviceSize offset, VkDeviceSize size, void** data)
{
    vkMapMemory(device, deviceMemory, offset, size, 0, data);
}

void Device::UnmapMemory(VkDeviceMemory deviceMemory)
{
    vkUnmapMemory(device, deviceMemory);
}

void Device::UpdateDescriptorSets(uint32_t descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites, uint32_t descriptorCopyCount, const VkCopyDescriptorSet* pDescriptorCopies)
{
    vkUpdateDescriptorSets(device, descriptorWriteCount, pDescriptorWrites, descriptorCopyCount, pDescriptorCopies);
}

void Device::WaitForFences(uint32_t fenceCount, const VkFence* fences)
{
    vkWaitForFences(device, fenceCount, fences, VK_TRUE, UINT64_MAX);
}

void Device::ResetFences(uint32_t fenceCount, const VkFence* fences)
{
    vkResetFences(device, fenceCount, fences);
}

void Device::WaitIdle()
{
    vkDeviceWaitIdle(device);
}
VkResult Device::AcquireNextImage(VkSwapchainKHR swapchain, uint64_t timeout, VkSemaphore semaphore, VkFence fence, uint32_t* image)
{
    return vkAcquireNextImageKHR(device, swapchain, timeout, semaphore, fence, image);
}