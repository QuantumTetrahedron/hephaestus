#pragma once
#include "../Gfx/vk.h"
#include <functional>

class Window
{
public:
	void create(uint32_t w, uint32_t h, const char* title);
	void cleanup();

	GLFWwindow* get() const;
	bool shouldClose();
	bool hasResized();

	bool resized = false;

	void enableCursor();
	void disableCursor();

	void close();

	void SetKeyCallback(const std::function<void(const Window&, int, int, int, int)>& fun);
	void SetCursorPosCallback(const std::function<void(const Window&, double, double)>& fun);
	void SetMouseButtonCallback(const std::function<void(const Window&, int, int, int)>& fun);

	uint32_t width, height;
private:
	GLFWwindow* window;

	std::function<void(const Window&, int, int, int, int)> keyCallback;
	std::function<void(const Window&, double, double)> cursorPosCallback;
	std::function<void(const Window&, int, int, int)> mouseButtonCallback;
};