#include "Window.h"
#include "../Gfx/Renderer.h"

static void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
    auto win = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    win->width = width;
    win->height = height;
    win->resized = true;
}

void Window::create(uint32_t w, uint32_t h, const char* title) {
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    window = glfwCreateWindow(w, h, title, nullptr, nullptr);

    glfwSetWindowUserPointer(window, this);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);

    width = w;
    height = h;
}

void Window::cleanup() {
    glfwDestroyWindow(window);
    glfwTerminate();
}

GLFWwindow* Window::get() const
{
    return window;
}

bool Window::shouldClose() {
    return glfwWindowShouldClose(window);
}

bool Window::hasResized()
{
    if (resized) {
        resized = false;
        return true;
    }
    else return false;
}

void Window::enableCursor()
{
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Window::disableCursor()
{
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Window::close()
{
    glfwSetWindowShouldClose(window, true);
}

void Window::SetKeyCallback(const std::function<void(const Window&, int, int, int, int)>& fun) {
    keyCallback = fun;
    glfwSetWindowUserPointer(window, (void*)this);
    glfwSetKeyCallback(window, [](GLFWwindow* win, int key, int scancode, int action, int mods) {
        auto t = (Window*)glfwGetWindowUserPointer(win);
        t->keyCallback(*t, key, scancode, action, mods);
    });
}

void Window::SetCursorPosCallback(const std::function<void(const Window&, double, double)>& fun) {
    cursorPosCallback = fun;
    glfwSetWindowUserPointer(window, (void*)this);
    glfwSetCursorPosCallback(window, [](GLFWwindow* win, double xPos, double yPos) {
        auto t = (Window*)glfwGetWindowUserPointer(win);
        t->cursorPosCallback(*t, xPos, yPos);
    });
}

void Window::SetMouseButtonCallback(const std::function<void(const Window&, int, int, int)>& fun) {
    mouseButtonCallback = fun;
    glfwSetWindowUserPointer(window, (void*)this);
    glfwSetMouseButtonCallback(window, [](GLFWwindow* win, int button, int action, int mods) {
        auto t = (Window*)glfwGetWindowUserPointer(win);
        t->mouseButtonCallback(*t, button, action, mods);
    });
}