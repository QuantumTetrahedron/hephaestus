#pragma once

#include <string>
#include <map>
#include <GLFW/glfw3.h>
#include "Window.h"
#include <vector>
#include <memory>
#include "MouseHandler.h"

class IniFile;
enum class Key;

class Input {
public:
    static float GetAxis(const std::string& name);
    static bool GetButton(Key key);
    static glm::vec2 GetMousePos();
    static glm::vec2 GetMouseOffset();
    static void DisableCursor();
    static void EnableCursor();

    static void QuitGame();

    static void AddAxis(const std::string& name, std::initializer_list<Key> positiveKeys, std::initializer_list<Key> negativeKeys);
    static void RemoveAxis(const std::string& name);
private:
    friend class App;
    static void Initialize(Window* window);

    static void ProcessKeyInput(int key, int state);
    static void ProcessMouseMove(float x, float y);
    static void ProcessMouseButton(int button, int action);
    static void Reset();

    static std::map<std::string, float> axes;

    static std::map<std::string, std::pair<std::vector<Key>, std::vector<Key>>> axisKeys;

    static std::vector<Key> pressedKeys;

    static Window* window;
};

enum class Key {
    n0 = GLFW_KEY_0,
    n1 = GLFW_KEY_1,
    n2 = GLFW_KEY_2,
    n3 = GLFW_KEY_3,
    n4 = GLFW_KEY_4,
    n5 = GLFW_KEY_5,
    n6 = GLFW_KEY_6,
    n7 = GLFW_KEY_7,
    n8 = GLFW_KEY_8,
    n9 = GLFW_KEY_9,
    A = GLFW_KEY_A,
    Apostrophe = GLFW_KEY_APOSTROPHE,
    B = GLFW_KEY_B,
    Backslash = GLFW_KEY_BACKSLASH,
    Backspace = GLFW_KEY_BACKSPACE,
    C = GLFW_KEY_C,
    CapsLock = GLFW_KEY_CAPS_LOCK,
    Comma = GLFW_KEY_COMMA,
    D = GLFW_KEY_D,
    Delete = GLFW_KEY_DELETE,
    Down = GLFW_KEY_DOWN,
    E = GLFW_KEY_E,
    Escape = GLFW_KEY_ESCAPE,
    End = GLFW_KEY_END,
    Enter = GLFW_KEY_ENTER,
    Equal = GLFW_KEY_EQUAL,
    F = GLFW_KEY_F,
    F1 = GLFW_KEY_F1,
    F2 = GLFW_KEY_F2,
    F3 = GLFW_KEY_F3,
    F4 = GLFW_KEY_F4,
    F5 = GLFW_KEY_F5,
    F6 = GLFW_KEY_F6,
    F7 = GLFW_KEY_F7,
    F8 = GLFW_KEY_F8,
    F9 = GLFW_KEY_F9,
    F10 = GLFW_KEY_F10,
    F11 = GLFW_KEY_F11,
    F12 = GLFW_KEY_F12,
    F13 = GLFW_KEY_F13,
    F14 = GLFW_KEY_F14,
    F15 = GLFW_KEY_F15,
    F16 = GLFW_KEY_F16,
    F17 = GLFW_KEY_F17,
    F18 = GLFW_KEY_F18,
    F19 = GLFW_KEY_F19,
    F20 = GLFW_KEY_F20,
    F21 = GLFW_KEY_F21,
    F22 = GLFW_KEY_F22,
    F23 = GLFW_KEY_F23,
    F24 = GLFW_KEY_F24,
    F25 = GLFW_KEY_F25,
    G = GLFW_KEY_G,
    GraveAccent = GLFW_KEY_GRAVE_ACCENT,
    H = GLFW_KEY_H,
    Home = GLFW_KEY_HOME,
    I = GLFW_KEY_I,
    Insert = GLFW_KEY_INSERT,
    J = GLFW_KEY_J,
    K = GLFW_KEY_K,
    KP0 = GLFW_KEY_KP_0,
    KP1 = GLFW_KEY_KP_1,
    KP2 = GLFW_KEY_KP_2,
    KP3 = GLFW_KEY_KP_3,
    KP4 = GLFW_KEY_KP_4,
    KP5 = GLFW_KEY_KP_5,
    KP6 = GLFW_KEY_KP_6,
    KP7 = GLFW_KEY_KP_7,
    KP8 = GLFW_KEY_KP_8,
    KP9 = GLFW_KEY_KP_9,
    KPAdd = GLFW_KEY_KP_ADD,
    KPDecimal = GLFW_KEY_KP_DECIMAL,
    KPDivide = GLFW_KEY_KP_DIVIDE,
    KPEnter = GLFW_KEY_KP_ENTER,
    KPEqual = GLFW_KEY_KP_EQUAL,
    KPMultiply = GLFW_KEY_KP_MULTIPLY,
    KPSubtract = GLFW_KEY_KP_SUBTRACT,
    L = GLFW_KEY_L,
    Last = GLFW_KEY_LAST,
    Left = GLFW_KEY_LEFT,
    LeftAlt = GLFW_KEY_LEFT_ALT,
    LeftBracket = GLFW_KEY_LEFT_BRACKET,
    LeftControl = GLFW_KEY_LEFT_CONTROL,
    LeftShift = GLFW_KEY_LEFT_SHIFT,
    LeftSuper = GLFW_KEY_LEFT_SUPER,
    M = GLFW_KEY_M,
    Menu = GLFW_KEY_MENU,
    Minus = GLFW_KEY_MINUS,
    N = GLFW_KEY_N,
    NumLock = GLFW_KEY_NUM_LOCK,
    O = GLFW_KEY_O,
    P = GLFW_KEY_P,
    PageDown = GLFW_KEY_PAGE_DOWN,
    PageUp = GLFW_KEY_PAGE_UP,
    Pause = GLFW_KEY_PAUSE,
    Period = GLFW_KEY_PERIOD,
    PrintScreen = GLFW_KEY_PRINT_SCREEN,
    Q = GLFW_KEY_Q,
    R = GLFW_KEY_R,
    Right = GLFW_KEY_RIGHT,
    RightAlt = GLFW_KEY_RIGHT_ALT,
    RightBracket = GLFW_KEY_RIGHT_BRACKET,
    RightControl = GLFW_KEY_RIGHT_CONTROL,
    RightShift = GLFW_KEY_RIGHT_SHIFT,
    RightSuper = GLFW_KEY_RIGHT_SUPER,
    S = GLFW_KEY_S,
    ScrollLock = GLFW_KEY_SCROLL_LOCK,
    Semicolon = GLFW_KEY_SEMICOLON,
    Slash = GLFW_KEY_SLASH,
    Space = GLFW_KEY_SPACE,
    T = GLFW_KEY_T,
    Tab = GLFW_KEY_TAB,
    U = GLFW_KEY_U,
    Up = GLFW_KEY_UP,
    V = GLFW_KEY_V,
    W = GLFW_KEY_W,
    World1 = GLFW_KEY_WORLD_1,
    World2 = GLFW_KEY_WORLD_2,
    X = GLFW_KEY_X,
    Y = GLFW_KEY_Y,
    Z = GLFW_KEY_Z
};
