#pragma once

#include <glm/glm.hpp>
#include <string>

class MouseHandler {
private:

    friend class Input;

    enum class mode {
        none, hud, world, all
    };

    static void ProcessButton(int button, int action);
    static void UpdateTarget();
    static void Initialize(const std::string& mode);
    static void SetMode(const std::string& newMode);
    static void Reset();

    //static std::shared_ptr<HUDColliderComponent> mouseHUDTarget;
    //static std::shared_ptr<ColliderComponent> mouseTarget;

    static bool mouseIsDown;

    static mode m;

    static bool mouseDisabled;
    static bool firstMouse;

    /** For enabled cursor **/
    static glm::vec2 mousePos;
    static glm::vec2 lastMousePos;

    /** For disabled cursor **/
    static glm::vec2 mouseOffset;
    static glm::vec2 lastMouseOffset;

    static glm::vec2 GetMousePos();
    static glm::vec2 GetMouseOffset();
    static void ProcessMouseMove(float x, float y);
    static void DisableCursor();
    static void EnableCursor();
    static void SetFirstMouse(float x, float y);

    //static std::shared_ptr<ColliderComponent> CastRay();
};

