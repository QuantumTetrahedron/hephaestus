#include "Input.h"

std::map<std::string, std::pair<std::vector<Key>, std::vector<Key>>> Input::axisKeys;
std::map<std::string, float> Input::axes;
std::vector<Key> Input::pressedKeys;
Window* Input::window;

float Input::GetAxis(const std::string& name) {
    return axes.at(name);
}

void Input::ProcessKeyInput(int key, int action) {
    for (auto& p : axes) {
        const std::vector<Key> positiveKeys = axisKeys.at(p.first).first;
        const std::vector<Key> negativeKeys = axisKeys.at(p.first).second;
        if (std::find(std::begin(positiveKeys), std::end(positiveKeys), (Key)key) != std::end(positiveKeys)) {
            if (action == GLFW_PRESS) p.second += 1;
            else if (action == GLFW_RELEASE) p.second -= 1;
        }
        else if (std::find(std::begin(negativeKeys), std::end(negativeKeys), (Key)key) != std::end(negativeKeys)) {
            if (action == GLFW_PRESS) p.second -= 1;
            else if (action == GLFW_RELEASE) p.second += 1;
        }
    }

    if (action == GLFW_PRESS) {
        pressedKeys.push_back((Key)key);
    }
}

void Input::ProcessMouseButton(int button, int action) {
    MouseHandler::ProcessButton(button, action);
}

void Input::Initialize(Window* win) {
    window = win;
    MouseHandler::Initialize("all");
}

bool Input::GetButton(Key key) {
    return !pressedKeys.empty() && std::find(std::begin(pressedKeys), std::end(pressedKeys), key) != std::end(pressedKeys);
}

void Input::Reset() {
    pressedKeys.clear();
    MouseHandler::Reset();
}

glm::vec2 Input::GetMousePos() {
    return MouseHandler::GetMousePos();
}

glm::vec2 Input::GetMouseOffset() {
    return MouseHandler::GetMouseOffset();
}

void Input::ProcessMouseMove(float x, float y) {
    MouseHandler::ProcessMouseMove(x, y);
}

void Input::DisableCursor() {
    MouseHandler::DisableCursor();
    window->disableCursor();
}

void Input::EnableCursor() {
    MouseHandler::EnableCursor();
    window->enableCursor();
}

void Input::QuitGame() {
    window->close();
}

void Input::AddAxis(const std::string& name, std::initializer_list<Key> positiveKeys,
    std::initializer_list<Key> negativeKeys) {
    axisKeys.emplace(name, std::make_pair(std::vector<Key>(positiveKeys), std::vector<Key>(negativeKeys)));
    axes.emplace(name, 0.0f);
}

void Input::RemoveAxis(const std::string& name) {
    axisKeys.erase(name);
    axes.erase(name);
}