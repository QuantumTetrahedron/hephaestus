
#include "MouseHandler.h"
#include "Window.h"

bool MouseHandler::mouseDisabled = false;
glm::vec2 MouseHandler::mousePos;
glm::vec2 MouseHandler::lastMousePos;
glm::vec2 MouseHandler::mouseOffset;
glm::vec2 MouseHandler::lastMouseOffset;
bool MouseHandler::firstMouse = true;
MouseHandler::mode MouseHandler::m = MouseHandler::mode::none;
//std::shared_ptr<HUDColliderComponent> MouseHandler::mouseHUDTarget;
//std::shared_ptr<ColliderComponent> MouseHandler::mouseTarget;
bool MouseHandler::mouseIsDown = false;

void MouseHandler::ProcessButton(int button, int action) {
    /*if (mouseIsDown && action == GLFW_RELEASE) {
        // same but mouse up
        mouseIsDown = false;
    }
    else if (!mouseIsDown && action == GLFW_PRESS) {
        mouseIsDown = true;
        if (m == mode::hud || m == mode::all) {
            if (mouseHUDTarget) {
                mouseHUDTarget->OnMouseDown(button);
                return;
            }
        }

        if (m == mode::world || m == mode::all) {
            auto c = CastRay();
            if (c) {
                c->OnMouseDown(button);
            }
        }
    }*/
}

void MouseHandler::Initialize(const std::string& mode) {
    SetMode(mode);
}

void MouseHandler::SetMode(const std::string& newMode) {
    if (newMode == "none")
        m = mode::none;
    else if (newMode == "hud")
        m = mode::hud;
    else if (newMode == "world")
        m = mode::world;
    else if (newMode == "all")
        m = mode::all;
}

void MouseHandler::Reset() {
    if (mouseDisabled) {
        mouseOffset = glm::vec2(0.0f);
    }

    UpdateTarget();

    /*if (SceneManager::IsSceneChanging()) {
        mouseHUDTarget = nullptr;
        mouseTarget = nullptr;
    }*/
}

glm::vec2 MouseHandler::GetMousePos() {
    return glm::vec2(0, 0);
    /*
    auto cam = Gfx::GetMainCamera();
    if (!cam) return glm::vec2(0.0f);
    return mouseDisabled ? glm::vec2(cam->GetWidth() / 2, cam->GetHeight() / 2) : mousePos;
    */
}

glm::vec2 MouseHandler::GetMouseOffset() {
    return mouseOffset;
}

void MouseHandler::ProcessMouseMove(float x, float y) {
    if (mouseDisabled) {
        if (firstMouse)
            SetFirstMouse(x, y);
        else {
            mouseOffset.x = x - lastMouseOffset.x;
            mouseOffset.y = lastMouseOffset.y - y;
            lastMouseOffset = glm::vec2(x, y);
        }
    }
    else {
        mousePos = glm::vec2(x, y);
    }
}

void MouseHandler::DisableCursor() {
    if (mouseDisabled) return;

    mouseDisabled = true;
    firstMouse = true;

    lastMousePos = mousePos;
    mousePos = glm::vec2(0.0f);

    mouseOffset = lastMouseOffset;
}

void MouseHandler::EnableCursor() {
    if (!mouseDisabled) return;
    mouseDisabled = false;

    mousePos = lastMousePos;

    lastMouseOffset = mouseOffset;
    mouseOffset = glm::vec2(0.0f);
}

void MouseHandler::SetFirstMouse(float x, float y) {
    if (mouseDisabled) {
        lastMouseOffset = glm::vec2(x, y);
        mouseOffset = glm::vec2(0.0f);
        firstMouse = false;
    }
}

void MouseHandler::UpdateTarget() {
    /*std::shared_ptr<HUDColliderComponent> old;
    if (mouseHUDTarget)
        old = mouseHUDTarget;
    mouseHUDTarget = Raycaster::HUDRay(GetMousePos());
    if (old) {
        if (!mouseHUDTarget) {
            old->OnMouseLeave();
            return;
        }

        if (old == mouseHUDTarget)
            return;
        else {
            old->OnMouseLeave();
            mouseHUDTarget->OnMouseEnter();
        }
    }
    else if (mouseHUDTarget) {
        mouseHUDTarget->OnMouseEnter();
    }

    std::shared_ptr<ColliderComponent> _old;
    if (mouseTarget) {
        _old = mouseTarget;
    }
    mouseTarget = CastRay();
    if (_old) {
        if (!mouseTarget) {
            _old->OnMouseLeave();
            return;
        }

        if (_old == mouseTarget)
            return;
        else {
            _old->OnMouseLeave();
            mouseTarget->OnMouseEnter();
        }
    }
    else if (mouseTarget) {
        mouseTarget->OnMouseEnter();
    }*/
}

/*
std::shared_ptr<ColliderComponent> MouseHandler::CastRay() {
    auto cam = Gfx::GetMainCamera();

    if (!cam) return nullptr;

    glm::vec3 position = cam->GetPosition();

    glm::vec2 rayOrigin = GetMousePos();

    double x = 2 * rayOrigin.x / cam->GetWidth() - 1;
    double y = 1 - 2 * rayOrigin.y / cam->GetHeight();

    glm::vec4 ray_clip = glm::vec4(x, y, -1.0, 1.0);
    glm::vec4 ray_eye = glm::inverse(cam->GetProjectionMatrix()) * ray_clip;
    ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
    glm::vec4 ray_wor = glm::inverse(cam->GetViewMatrix()) * ray_eye;
    glm::vec3 direction = glm::vec3(ray_wor.x, ray_wor.y, ray_wor.z);
    direction = glm::normalize(direction);

    return Raycaster::WorldRay(position, direction);
}*/
