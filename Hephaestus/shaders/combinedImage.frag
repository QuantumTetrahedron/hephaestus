#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(set = 2, binding = 0) uniform sampler2D image1;
layout(set = 2, binding = 1) uniform sampler2D image2;

void main(){
	vec3 color1 = texture(image1, fragTexCoord).rgb;
	vec3 color2 = texture(image2, fragTexCoord).rgb;

	vec3 mixedColor = mix(color1, color2, 0.5);

	outColor = vec4(mixedColor, 1.0);
}