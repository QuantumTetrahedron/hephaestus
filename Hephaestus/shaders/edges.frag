#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(set = 2, binding = 0) uniform sampler2D diffuse;

void main(){
	vec2 pixel_size = 1.0 / vec2(textureSize(diffuse, 0));

	vec3 total = 9 * texture(diffuse, fragTexCoord).rgb;
	for(int y = -1; y <= 1; y++){
		for(int x = -1; x <= 1; x++){
			vec2 pixel_off = vec2(float(x), float(y));
			vec3 tex = texture(diffuse, fragTexCoord + pixel_off * pixel_size).rgb;
			total -= tex;
		}
	}
    

	outColor = vec4(total, 1.0);
}