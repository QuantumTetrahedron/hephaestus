#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform GlobalUniforms {
	mat4 view;
	mat4 proj;
} glob;

layout(set = 1, binding = 0) uniform ObjectUniforms {
	mat4 model;
} obj;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec2 fragTexCoord;

void main(){
	gl_Position = glob.proj * glob.view * obj.model * vec4(inPosition, 1.0);
	fragTexCoord = vec2(inTexCoord.x, 1.0 - inTexCoord.y);
}