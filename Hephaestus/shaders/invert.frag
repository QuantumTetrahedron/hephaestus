#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(set = 2, binding = 0) uniform sampler2D diffuse;

void main(){
	vec3 total = texture(diffuse, fragTexCoord).rgb;
	outColor = vec4(vec3(1.0) - total, 1.0);
}