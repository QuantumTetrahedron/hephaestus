#include "Core/App.h"
#include "Resources/ResourceManager.h"
#include "Input/Input.h"

int main() {
    App app;

    AppInfo appInfo;
    appInfo.windowWidth = 1600;
    appInfo.windowHeight = 900;
    appInfo.title = "Vulkan";
    appInfo.startScene = "main";
    appInfo.vortexPath = "vortex/deferred.vx";

    try {
        app.Initialize(appInfo);

        ResourceManager::LoadShader("shaders/vert.spv", "shaders/frag.spv", "default");
        ResourceManager::LoadShader("shaders/vert.spv", "shaders/colorInvert.spv", "colorInvert");
        ResourceManager::LoadShader("shaders/vert.spv", "shaders/multitarget.frag.spv", "multitargetTest");

        ResourceManager::LoadShader("shaders/gBuffer.vert.spv", "shaders/gBuffer.frag.spv", "gBuffer");
        ResourceManager::LoadShader("shaders/deferred.vert.spv", "shaders/deferred.frag.spv", "deferred");
        ResourceManager::LoadShader("shaders/postprocess.vert.spv", "shaders/postprocess.frag.spv", "postprocessDefault");

        ResourceManager::LoadShader("shaders/postprocess.vert.spv", "shaders/edges.frag.spv", "edges");
        ResourceManager::LoadShader("shaders/postprocess.vert.spv", "shaders/invert.frag.spv", "invert");
        ResourceManager::LoadShader("shaders/postprocess.vert.spv", "shaders/combinedImage.frag.spv", "combined");

        ResourceManager::LoadModel("models/backpack/backpack.obj", "backpack");
        ResourceManager::LoadModel("models/nanosuit/nanosuit.obj", "nanosuit");
        ResourceManager::LoadModel("models/Cube/Cube.obj", "cube");
        ResourceManager::LoadModel("models/Base/Base.obj", "base");

        Input::DisableCursor();
        Input::AddAxis("Horizontal", { Key::D, Key::Right }, { Key::A, Key::Left });
        Input::AddAxis("Vertical", { Key::W, Key::Up }, { Key::S, Key::Down });

        app.MainLoop();

        app.Cleanup();
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}