#pragma once

#include "Core/BehaviourComponent.h"

class RotatorComponent : public BehaviourComponent {

public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

private:
    float speed;
    std::string rotatedObject;
    Transform* toRotate;

    bool LoadFromFile(const IniFile& file) override;

    COMPONENT(RotatorComponent)
};