#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform GlobalUniforms {
	mat4 view;
	mat4 proj;
	vec3 viewPos;
} glob;

layout(set = 1, binding = 0) uniform ObjectUniforms {
	mat4 model;
} obj;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoords;

layout(location = 0) out vec3 FragPos;
layout(location = 1) out vec2 TexCoords;
layout(location = 2) out vec3 Normal;

void main(){
	vec4 worldPos = obj.model * vec4(inPosition, 1.0);
	FragPos = worldPos.xyz;
	TexCoords = vec2(inTexCoords.x, 1.0 - inTexCoords.y);

	mat3 normalMatrix = transpose(inverse(mat3(obj.model)));
	Normal = normalMatrix * inNormal;

	gl_Position = glob.proj * glob.view * worldPos;
}