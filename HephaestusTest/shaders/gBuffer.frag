#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 gPosition;
layout(location = 1) out vec4 gNormal;
layout(location = 2) out vec4 gAlbedoSpec;

layout(location = 0) in vec3 FragPos;
layout(location = 1) in vec2 TexCoords;
layout(location = 2) in vec3 Normal;

layout(set = 2, binding = 0) uniform sampler2D image;

void main(){
	gPosition = vec4(FragPos, 1.0);
	gNormal = vec4(normalize(Normal), 1.0);
	gAlbedoSpec.rgb = texture(image, TexCoords).rgb;
	gAlbedoSpec.a = 1.0; //texture(images[1], TexCoords).r;
}