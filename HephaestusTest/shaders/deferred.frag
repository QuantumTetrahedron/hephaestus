#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 fragTexCoord;
layout(location = 1) in vec3 viewPosition;

layout(set = 2, binding = 0) uniform sampler2D images[3];

struct Light {
	vec3 Position;
	vec3 Color;
};

void main(){
	Light light;
	light.Position = vec3(-2.0, 2.0, 1.0);
	light.Color = vec3(1.0);

	vec3 viewPos = viewPosition;

	vec3 FragPos = texture(images[0], fragTexCoord).rgb;
	vec3 Normal = texture(images[1], fragTexCoord).rgb;
	vec3 Diffuse = texture(images[2], fragTexCoord).rgb;
	float Specular = texture(images[2], fragTexCoord).a;

	vec3 lighting = Diffuse * 0.1;
	vec3 viewDir = normalize(viewPos - FragPos);
	
	vec3 lightDir = normalize(light.Position - FragPos);
	vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * light.Color;

	vec3 halfwayDir = normalize(lightDir + viewDir);
	float spec = pow(max(dot(Normal, halfwayDir), 0.0), 16.0);
	vec3 specular = spec * Specular * light.Color;

	lighting += diffuse + specular;

	outColor = vec4(lighting, 1.0);
}