#version 450

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(set = 0, binding = 0) uniform GlobalUniforms {
	mat4 view;
	mat4 proj;
	vec3 viewPos;
} glob;

layout (location = 0) out vec2 fragTexCoord;
layout (location = 1) out vec3 viewPosition;

void main() 
{
	gl_Position = vec4(inPosition, 1.0);
	fragTexCoord = inTexCoord;
	viewPosition = glob.viewPos;
}