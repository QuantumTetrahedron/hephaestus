#include "CameraController.h"
#include "Input/Input.h"
#include "Gfx/Renderer.h"

void CameraController::Start() {
    camera = GetParent()->GetComponent<CameraComponent>();
    camera->Activate();
    currentAngle = 0;
}

void CameraController::Update(float dt) {
    currentAngle += dt * 90;
    float xPos = 5 * glm::sin(glm::radians(currentAngle));
    float zPos = 5 * glm::cos(glm::radians(currentAngle));
    float yPos = 3;

    glm::vec3 pos(xPos, yPos, zPos);

    glm::vec3 front = glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f) - pos);

    camera->SetFront(front);
    
    parent.lock()->transform.position = pos;

    static bool _switch = false;
    if (Input::GetButton(Key::Q)) {
        _switch = !_switch;
        Renderer::SetVar("switch", _switch);
    }

    if (Input::GetButton(Key::n1)) {
        Renderer::SetVar("drawMode", 0);
    }

    if (Input::GetButton(Key::n2)) {
        Renderer::SetVar("drawMode", 1);
    }

    if (Input::GetButton(Key::n3)) {
        Renderer::SetVar("drawMode", 2);
    }

    if (Input::GetButton(Key::n4)) {
        Renderer::SetVar("drawMode", 3);
    }
}

bool CameraController::LoadFromFile(const IniFile& file) 
{
    if (!file.GetValue("speed", speed)) {
        std::cerr << "Missing speed parameter from CameraController" << std::endl;
        return false;
    }

    if (!file.GetValue("mouseSensitivity", mouseSensitivity)) {
        std::cerr << "Missing mouseSensitivity parameter from CameraController" << std::endl;
        return false;
    }

    return true;
}