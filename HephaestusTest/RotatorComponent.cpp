#include "RotatorComponent.h"
#include "Core/ObjectManager.h"
#include "Utils/IniFile.h"

void RotatorComponent::Start() {
    auto obj = ObjectManager::GetObject(rotatedObject);
    if (obj) {
        toRotate = &obj->transform;
    }
    else {
        toRotate = nullptr;
    }
}

void RotatorComponent::Update(float dt) {
    if (toRotate) {
        toRotate->rotation.y += dt * speed * 30;
    }
}

void RotatorComponent::OnLeave() {
}

bool RotatorComponent::LoadFromFile(const IniFile& file) {
    file.GetValue("speed", speed, 1.0f);
    file.RequireValue("rotatedObject", rotatedObject);
    return true;
}
