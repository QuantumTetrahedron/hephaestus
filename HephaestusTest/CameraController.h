#pragma once

#include "Core/BehaviourComponent.h"
#include "Core/CameraComponent.h"

class CameraController : public BehaviourComponent {
public:
	void Start() override;
	void Update(float dt) override;

	void OnLeave() override {}

	bool LoadFromFile(const IniFile& file) override;
private:
	float speed;
	float mouseSensitivity;
	float currentAngle;

	std::shared_ptr<CameraComponent> camera;

	COMPONENT(CameraController)
};