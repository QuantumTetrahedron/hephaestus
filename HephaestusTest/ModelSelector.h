#pragma once


#include "Core/BehaviourComponent.h"
#include "Core/RenderComponent.h"
#include "Core/ObjectFactory.h"

class ModelSelector : public BehaviourComponent {
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void ChangeModel(int direction);

protected:
    std::vector<std::string> models;

    int currentModel;

    std::string objName;
    std::shared_ptr<RenderComponent> obj;

    bool LoadFromFile(const IniFile& file) override;

    COMPONENT(ModelSelector)
};